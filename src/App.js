import React from "react";
import "./App.css";
import Header from "./common/header";
import { Switch, Route } from "react-router-dom";
import Home from "./components/home/home";
import Forgot from "./components/forgotpassword/forgot";
import ResetPassword from "./components/forgotpassword/resetpassword";
import ProtectedRoute from "./components/protectedRoute";
import Tambola from "./components/tambola/tambola";
import ChangePassword from "./components/changepassword/changepassword";
import BuyOneTicket from "./components/tambola/buyoneticket/buyoneticket";
import BuyTwoTickets from "./components/tambola/buytwoticket/buytwoticket";
import Transactions from "./components/transactions/transactions";
import AddFunds from "./components/addfunds/addfunds";
import LastgameWinners from "./components/tambola/lastgamewinners";
import Withdraw from "./components/withdraw/withdraw";
import buyoneticketnew from "./components/tambola/buyoneticket/buyoneticketnew";
import Lotto from "./components/tambola/lotto";
import Bingo from "./components/bingo/bingo";
import Index from "./components/bingo/index";
import RouletteTable from "./components/roulette/table1";
// import Win from "./components/bingo/win";

function App() {
  return (
    <React.Fragment>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <ProtectedRoute exact path="/home" component={Home} />
        <Route exact path="/forgot" component={Forgot} />
        <Route exact path="/reset/:id" component={ResetPassword} />
        <Route exact path="/tambola" component={Tambola} />
        <ProtectedRoute exact path="/lotto" component={Lotto} />
        <ProtectedRoute exact path="/roulette" component={RouletteTable} />

        <ProtectedRoute exact path="/index" component={Index} />
        {/* <ProtectedRoute exact path="/win" component={Win} /> */}
        <ProtectedRoute exact path="/bingo" component={Bingo} />
        <ProtectedRoute exact path="/buyoneticket" component={BuyOneTicket} />
        <ProtectedRoute
          exact
          path="/buyoneticketnnew"
          component={buyoneticketnew}
        />
        <ProtectedRoute exact path="/buytwoticket" component={BuyTwoTickets} />
        <ProtectedRoute
          exact
          path="/changepassword"
          component={ChangePassword}
        />
        <ProtectedRoute exact path="/transactions" component={Transactions} />
        <ProtectedRoute exact path="/addfunds" component={AddFunds} />
        <ProtectedRoute exact path="/withdraw" component={Withdraw} />
        <Route exact path="/lastgamewinners" component={LastgameWinners} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
