import React, { Component } from 'react';
import logo from '../assets/images/mck-logo.png';

class Footer extends Component {
	state = {};
	render() {
		return (
			<React.Fragment>
				<footer>
					<div class='footer-top'>
						<div class='footer-top-inner'>
							<div class='container'>
								<div class='row'>
									<div class='col-xs-6 col-sm-2 col-md-2'>
										<div class='footer-link'>
											<h3>Games</h3>
											<ul class='linkarea'>
												<li>
													<a>Quiz</a>
												</li>
												<li>
													<a>Tambola</a>
												</li>
												<li>
													<a>Cricket</a>
												</li>
												<li>
													<a>Sudoku</a>
												</li>
												<li>
													<a>5 Minute Games</a>
												</li>
											</ul>
										</div>
									</div>
									<div class='col-xs-6 col-sm-2 col-md-2'>
										<div class='footer-link'>
											<h3>Other Sections</h3>
											<ul class='linkarea'>
												<li>
													<a>Movie Reviews</a>
												</li>
												<li>
													<a>Product Reviews</a>
												</li>
												<li>
													<a>Horoscope</a>
												</li>
												<li>
													<a>Invite Friends</a>
												</li>
											</ul>
										</div>
									</div>
									<div class='col-xs-6 col-sm-2 col-md-2'>
										<div class='footer-link'>
											<h3>Rewards</h3>
											<ul class='linkarea'>
												<li>
													<a>Courier Details</a>
												</li>
												<li>
													<a>Cheque Requests</a>
												</li>
												<li>
													<a>Recharge Requests</a>
												</li>
												<li>
													<a>Rewards Store</a>
												</li>
											</ul>
										</div>
									</div>
									<div class='col-xs-6 col-sm-2 col-md-2'>
										<div class='footer-link'>
											<h3>Social Media</h3>
											<ul class='linkarea'>
												<li>
													<a>Facebook</a>
												</li>
												<li>
													<a>Twitter</a>
												</li>
											</ul>
										</div>
									</div>
									<div class='col-xs-6 col-sm-2 col-md-2'>
										<div class='footer-link'>
											<h3>Information</h3>
											<ul class='linkarea'>
												<li>
													<a>HOW IT WORKS </a>
												</li>
												<li>
													<a>FAQs</a>
												</li>
												<li>
													<a>Sitemap</a>
												</li>
											</ul>
										</div>
									</div>
									<div class='col-xs-6 col-sm-2 col-md-2'>
										<div class='footer-link'>
											<h3>Support</h3>
											<ul class='linkarea'>
												<li>
													<a>Contact Us</a>
												</li>
												<li>
													<a>Advertise with us</a>
												</li>
												<li>
													<a>Privacy Policy</a>
												</li>
												<li>
													<a>Terms of Use</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class='footer-bottom'>
						<div class='footer-bottom-inner'>
							<div class='container'>
								<div class='row'>
									<div class='col-sm-3 col-md-2'>
										<div class='logo-footer'>
											<a href='index.html'>
												<img src={logo} alt='CG HOUSIE' />
											</a>
										</div>
									</div>
									<div class='col-sm-9 col-md-10'>
										<p>Copyright &copy; 2020 CG HOUSIE</p>
										<p>
											Trademarks CG HOUSIE are the property of CG HOUSIE all rights reserved.
											Patents Pending.{' '}
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</React.Fragment>
		);
	}
}

export default Footer;
