import React, { Component } from "react";
import logo from "../assets/images/logo.png";
import responsivebtn from "../assets/images/responsive-btn.png";
import smalllogo from "../assets/images/mck-logo-small.png";
import Navbar from "./navbar";
import Modal from "react-responsive-modal";
import Register from "../components/register/register";
import Login from "../components/login/login";
import { connect } from "react-redux";
import update_auth from "../redux/actions/authActions";
import auth from "../services/authService";
import avatar from "../assets/images/avatar.png";
import { Link } from "react-router-dom";
class Header extends Component {
  state = {
    registeropen: false,
    loginopen: false,
    logoutdrop: false,
  };
  componentDidMount = () => {
    update_auth();
  };

  onOpenregisterModal = () => {
    this.setState({ registeropen: true });
  };

  onCloseregisterModal = () => {
    this.setState({ registeropen: false });
  };
  onOpenloginModal = () => {
    this.setState({ loginopen: true });
  };

  onCloseloginModal = () => {
    this.setState({ loginopen: false });
  };
  logoutdropdown = async () => {
    await this.setState({ logoutdrop: !this.state.logoutdrop });
  };
  logout = () => {
    auth.logout();
    window.location = "/";
  };
  render() {
    return (
      <React.Fragment>
        <div id="container">
          <div className="mck-mobile-header clearfix">
            {/* <div className="menu-btn">
							<img
								src={responsivebtn}
								width="30"
								height="22"
								alt="My Cash Kit"
							/>
						</div> */}

            <div>
              <div className="mb-logo ml-2">
                <a>
                  <h4 style={{ color: "white" }}>JUAN-GAMEZONE</h4>
                </a>
              </div>
              {!auth.getCurrentUser() ? (
                <div className="signin-signup">
                  <ul className="clearfix">
                    <li
                      className="cursor-pointer "
                      onClick={this.onOpenregisterModal}
                    >
                      <a>
                        <i className="fa fa-user-plus" aria-hidden="true" />{" "}
                        Register
                      </a>
                    </li>
                    <li
                      className="cursor-pointer ml-4"
                      onClick={this.onOpenloginModal}
                    >
                      <a>
                        <i className="fa fa-sign-in-alt" aria-hidden="true" />{" "}
                        Sign in
                      </a>
                    </li>
                  </ul>
                </div>
              ) : (
                <div className="user-right">
                  <div className="navbar-custom-menu clearfix">
                    <ul className="nav navbar-nav">
                      {/* <li className="prblem-report">
												<a target="_blank">
													{" "}
													<span className="report">
														<i className="fa fa-question"></i>Report a Problem
													</span>
												</a>
											</li> */}

                      <li
                        onClick={this.logoutdropdown}
                        className="dropdown user user-menu"
                      >
                        {" "}
                        <a className="dropdown-toggle clearfix">
                          <img
                            src={avatar}
                            className="user-image"
                            alt="User Image"
                          />
                          <span className="hidden-xs username capitalize">
                            <i className="fa fa-caret-down spaceset m-0" />
                          </span>{" "}
                        </a>
                        {this.state.logoutdrop ? (
                          <ul className="dropdown-menu">
                            <li>
                              <Link to="/lotto">
                                <i className="fa fa-money-check" />
                                Lotto
                              </Link>
                            </li>
                            <li>
                              <Link to="/roulette">
                                <i className="fa fa-money-check" />
                                Roulette
                              </Link>
                            </li>
                            {/* <li>
                              <Link to="/index">
                                <i className="fa fa-money-check" />
                                Bingo
                              </Link>
                            </li>  */}
                            {/* <li>
                              <Link to="/tambola">
                                <i className="fa fa-money-check" />
                                Housie
                              </Link>
                            </li>
                            <li>
                              <Link to="/addfunds">
                                <i className="fa fa-money-check" />
                                Add Funds
                              </Link>
                            </li>
                            <li>
                              <Link to="/transactions">
                                <i className="fa fa-money-check" />
                                Transactions
                              </Link>
                            </li>

                            <li>
                              <Link to="/lastgamewinners">
                                <i className="fa fa-money-check" />
                                Last Win
                              </Link>
                            </li>
                            <li>
                              <Link to="/withdraw">
                                <i className="fa fa-money-check" />
                                Withdraw
                              </Link>
                            </li> */}

                            <li>
                              <Link to="/changepassword">
                                <i className="fa fa-cog" /> Change Password
                              </Link>
                            </li>

                            <li onClick={() => this.logout()}>
                              <a>
                                <i className="fa fa-sign-out-alt" /> Log Out
                              </a>
                            </li>
                          </ul>
                        ) : null}
                      </li>
                    </ul>
                  </div>
                </div>
              )}
            </div>
          </div>
          <header className="mck-desktop-header">
            <div className="page_header">
              <div className="container">
                <div className="logo-left">
                  <a>
                    {/* <img src={logo} alt="My Cash Kit" /> */}
                    <h4 style={{ color: "white" }}>JUAN-GAMEZONE</h4>
                  </a>
                </div>
                {!auth.getCurrentUser() ? (
                  <div className="signin-signup">
                    <ul className="clearfix">
                      <li
                        className="cursor-pointer "
                        onClick={this.onOpenregisterModal}
                      >
                        <a>
                          <i className="fa fa-user-plus " aria-hidden="true" />{" "}
                          Register
                        </a>
                      </li>
                      <li
                        className="cursor-pointer"
                        onClick={this.onOpenloginModal}
                      >
                        <a className="bg-orange-light">
                          <i className="fa fa-sign-in-alt" aria-hidden="true" />{" "}
                          Sign in
                        </a>
                      </li>
                    </ul>
                  </div>
                ) : (
                  <div className="user-right">
                    <div className="navbar-custom-menu clearfix">
                      <ul className="nav navbar-nav">
                        <li className="prblem-report">
                          <Link to="/lotto">
                            {" "}
                            <span className="report">Lotto</span>
                          </Link>
                        </li>
                        <li className="prblem-report">
                          <Link to="/roulette">
                            {" "}
                            <span className="report">Roulette</span>
                          </Link>
                        </li>
                        {/* <li className="prblem-report">
													<Link to="/index">
														{" "}
														<span className="report">Bingo</span>
													</Link>
												</li> */}
                        {/* <li className="prblem-report">
                          <Link to="/tambola">
                            {" "}
                            <span className="report">Housie</span>
                          </Link>
                        </li>
                        <li className="prblem-report">
                          <Link to="/addfunds">
                            {" "}
                            <span className="report">Add Funds</span>
                          </Link>
                        </li>
                        <li className="prblem-report">
                          <Link to="/withdraw">
                            {" "}
                            <span className="report">Withdraw</span>
                          </Link>
                        </li>
                        <li className="prblem-report">
                          <Link to="/transactions">
                            {" "}
                            <span className="report">Transactions</span>
                          </Link>
                        </li>
                        <li className="prblem-report">
                          <Link to="/lastgamewinners">
                            <span className="report">Last Win</span>
                          </Link>
                        </li> */}
                        <li
                          onClick={this.logoutdropdown}
                          className="dropdown user user-menu"
                        >
                          {" "}
                          <a className="dropdown-toggle clearfix">
                            <img
                              src={avatar}
                              className="user-image"
                              alt="User Image"
                            />
                            <span className="hidden-xs username capitalize">
                              {this.props.auth.name}{" "}
                              <i className="fa fa-caret-down spaceset" />
                            </span>{" "}
                          </a>
                          {this.state.logoutdrop ? (
                            <ul className="dropdown-menu">
                              <li>
                                <Link to="/changepassword">
                                  <i className="fa fa-cog" /> Change Password
                                </Link>
                              </li>

                              <li onClick={() => this.logout()}>
                                <a>
                                  <i className="fa fa-sign-out-alt" /> Log Out
                                </a>
                              </li>
                            </ul>
                          ) : null}
                        </li>
                      </ul>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </header>
        </div>
        {/* Register Modal */}
        <Modal
          open={this.state.registeropen}
          onClose={this.onCloseregisterModal}
          showCloseIcon={false}
          center
        >
          <Register
            loginopen={this.onOpenloginModal}
            close={this.onCloseregisterModal}
          />
        </Modal>
        <Modal
          open={this.state.loginopen}
          onClose={this.onCloseloginModal}
          showCloseIcon={false}
          center
        >
          <Login close={this.onCloseloginModal} />
        </Modal>
        {/* <Navbar /> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps)(Header);
