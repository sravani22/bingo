<tbody>
	<tr>
		<td className="pt-3">
			{" "}
			<br></br>
			{moment(getbingo.date).format("D MMM YYYY hh:mm A")}
		</td>
		<td className="pt-3">
			{" "}
			<br></br> {getbingo ? getbingo.game.gameid : <div>Please Wait..</div>}
		</td>
		<td>
			{getbingo ? (
				<img
					src={require(`../../assets/images/${getbingo.game.pattern}.png`)}
					alt="img"
				/>
			) : (
				<div className="pt-4">Please Wait..</div>
			)}
		</td>

		<td className="pt-4">
			{" "}
			<div className="nums_circle">
				{" "}
				{getbingo ? getbingo.user : this.state.card}{" "}
			</div>{" "}
		</td>
		<td className="pt-2">
			<div className="col-lg-12 px-4">
				<div className="input-group">
					{this.state.card <= 0 ? (
						<span className="input-group-btn">
							<button
								type="button"
								className="quantity-left-minus btn btn-danger btn-number not-allowed"
								// onClick={this.decrement}
								disabled={true}
							>
								<span className="fa fa-minus"></span>
							</button>
						</span>
					) : (
						<span className="input-group-btn">
							<button
								type="button"
								className="quantity-left-minus btn btn-danger btn-number "
								onClick={this.decrement}
							>
								<span className="fa fa-minus"></span>
							</button>
						</span>
					)}
					<input
						className="form-control input-number"
						readOnly
						value={this.state.card}
					/>
					{this.state.card === 8 ? (
						<span className="input-group-btn">
							<button
								type="button"
								className="quantity-right-plus btn btn-success btn-number not-allowed"
								disabled={true}
							>
								<span className="fa fa-plus"></span>
							</button>
						</span>
					) : (
						<span className="input-group-btn">
							<button
								type="button"
								className="quantity-right-plus btn btn-success btn-number"
								onClick={this.increment}
							>
								<span className="fa fa-plus"></span>
							</button>
						</span>
					)}
				</div>

				<button
					className="btn btn-play d-block w-100 mt-2"
					onClick={this.openBingo}
					disabled={this.state.disable}
				>
					{" "}
					Buy Ticket
				</button>
			</div>
			{/* <div className="pt-2">Please Wait..</div> */}
		</td>
		<td>
			{getbingo ? (
				getbingo.game ? (
					getbingo.game.status === "pending" ? (
						<button
							className="btn btn-green bg-orange"
							// onClick={this.openBingo}
							disabled={this.state.disable}
						>
							Pending
						</button>
					) : getbingo ? (
						<button
							style={{ background: "#28a745" }}
							className="btn btn-green bg-orange"
							onClick={this.resumeBingo}
							disabled={this.state.disable}
						>
							{" "}
							Resume
						</button>
					) : (
						<div className="pt-4">Please Wait..</div>
					)
				) : (
					<div className="pt-4">Please Wait..</div>
				)
			) : (
				<div className="pt-4">Please Wait..</div>
			)}
		</td>
	</tr>
</tbody>;
