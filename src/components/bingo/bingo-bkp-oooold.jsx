import React, { Component } from "react";
import { connect } from "react-redux";
import auth from "../../services/authService";
import ReactCountdownClock from "react-countdown-clock";
import Moment from "react-moment";
import { toast } from "react-toastify";
import clock from "../../assets/images/clock1.png";
import get_bingogame from "../../redux/actions/getbingoActions";
import bingo from "../../services/bingoservice";
import { WinPatterns } from "./winpatterns";

class Bingo extends Component {
  state = {
    gamenum: [],
    num: [],
    ct: true,
    userPattern: [],
    ticket: [],
    usernums: [],
    winner: "",
    claimdiagonal: "Claim Diagonal",
    claim1strow: "Claim 1st Row",
    claim2ndcol: "Claim 2nd Column",
    claim2rows: "Claim 1,4 - Rows",
    claim3rows: "Claim 1,3,5 - Rows",
    bingobtndisable: false,
    // diagonalbtndisable: false,
    // twocolbtndisable: false,
    // rowbtndisable: false,
    // tworowsbtndisable: false,
    // threerowsbtndisable: false,
    // fourrowsbtndisable: false,
    isbingo: false,
    patternmap: [],
    loader: false,
  };

  componentDidMount = async () => {
    const { getbingo } = this.props;
    if (!this.state.userPattern.length && !this.state.usernums.length) {
      setTimeout(async () => {
        await localStorage.setItem("gameid", this.props.getbingo.gameid);
        const gameId = await localStorage.getItem("gameid");
        // const selectedids = await localStorage.getItem("selectedids");
        // const usernumbs = await localStorage.getItem("usernums");
        // var localuser = [];

        // localuser.push(parseFloat(usernumbs));

        // console.log(localuser);
        // if (
        //   localuser &&
        //   localuser.length &&
        //   selectedids &&
        //   selectedids.length
        // ) {
        //   await this.setState({
        //     usernums: localuser,
        //     userPattern: selectedids,
        //   });
        //   console.log(selectedids, usernumbs);
        // }
      }, 3000);
    }
    // colocalStorage.getItem("selectedids");
    // localStorage.getItem("usernums");
    // if (!this.state.userPattern) {
    //   const usernums = await localStorage.getItem("userselectednums");
    //   console.log(usernums);
    // }
    if (!getbingo) {
      await get_bingogame();
    }
    setTimeout(() => {
      if (!getbingo.status === "resume") {
      }
    }, 3000);
    const { data } = await bingo.gettickets();
    try {
      if (data.length < 1) {
        this.props.history.push("/index");
      }
      await this.setState({ ticket: data });
    } catch (error) {}

    const user = auth.getCurrentUser();
  };
  idValue = async (e) => {
    if (
      this.state.gamenum.includes(parseFloat(e.target.dataset.value)) &&
      !this.state.usernums.includes(parseFloat(e.target.dataset.value))
    ) {
      const selectnums = this.state.usernums;
      selectnums.push(parseFloat(e.target.dataset.value));
      await this.setState({
        userPattern: [...this.state.userPattern, parseFloat(e.target.id)],
        usernums: selectnums,
      });

      // localStorage.setItem("gameid", this.props.getbingo.gameid);
      localStorage.setItem("selectedids", this.state.userPattern);
      localStorage.setItem("usernums", this.state.usernums);

      // console.log(this.state.userPattern);
      // localStorage.setItem(
      //   "userselectednums",
      //   this.state.userPattern,
      //   ...this.state.userPattern
      // );
    }
  };
  nextnum = async () => {
    await this.setState({ ct: false });
    const gamenum = { gameno: this.props.getbingo.gameid };
    const { data } = await bingo.drawnumbers(gamenum);
    if (data.winners.some((win) => win.winpat === "Bingo")) {
      await get_bingogame();
      data.winners.map(async (win) => {
        if (win.winpat === "Bingo") {
          var nam = ` ${win.name} ! `;
          await this.setState({ winner: nam, bingobtndisable: true });
          console.log(this.state.winner);
          setTimeout(() => {
            this.props.history.push({ pathname: "/index" });
          }, 7000);
          // return win;
        }
      });
    }
    if (data.winners.some((win) => win.winpat === "Diagonal")) {
      var winname = data.winners.map(async (win) => {
        if (win.winpat === "Diagonal") {
          var nam = `Diagonal Won By ${win.name} ! `;
          await this.setState({ claimdiagonal: nam, diagonalbtndisable: true });
          return win;
        }
      });
    }
    if (data.winners.some((win) => win.winpat === "Row")) {
      var winname = data.winners.map(async (win) => {
        if (win.winpat === "Row") {
          var nam = `1st Row Won By ${win.name} ! `;
          await this.setState({ claim1strow: nam, rowbtndisable: true });
          return win;
        }
      });
    }

    if (data.winners.some((win) => win.winpat === "Two rows")) {
      var winname = data.winners.map(async (win) => {
        if (win.winpat === "Two rows") {
          var nam = `1,4 Rows Won By ${win.name} ! `;
          await this.setState({ claim2rows: nam, tworowsbtndisable: true });
          return win;
        }
      });
    }

    if (data.winners.some((win) => win.winpat === "Three rows")) {
      var winname = data.winners.map(async (win) => {
        if (win.winpat === "Three rows") {
          var nam = `1,3,5 Rows Won By ${win.name} ! `;
          await this.setState({
            claim3rows: nam,
            threerowsbtndisable: true,
          });
          return win;
        }
      });
    }
    if (data.winners.some((win) => win.winpat === "Column")) {
      var winname = data.winners.map(async (win) => {
        if (win.winpat === "Column") {
          var nam = `Column Won By ${win.name} ! `;
          await this.setState({
            claim2ndcol: nam,
            twocolbtndisable: true,
          });
          return win;
        }
      });
    }
    await this.setState({ ct: true });
    await this.setState({
      num: [data.drawnum.slice(-1)[0]],
      gamenum: data.drawnum,
    });
    if (this.state.num >= 1 && this.state.num <= 15) {
      this.selectnum1();
    } else if (this.state.num > 15 && this.state.num <= 30) {
      this.selectnum2();
    } else if (this.state.num > 31 && this.state.num <= 45) {
      this.selectnum3();
    } else if (this.state.num > 46 && this.state.num <= 60) {
      this.selectnum4();
    } else if (this.state.num > 61 && this.state.num <= 75) {
      this.selectnum5();
    }
  };

  selectnum1 = () => {
    var rows = [];
    for (var i = 1; i <= 15; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum2 = () => {
    var rows = [];
    for (var i = 16; i <= 30; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum3 = () => {
    var rows = [];
    for (var i = 31; i <= 45; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum4 = () => {
    var rows = [];
    for (var i = 46; i <= 60; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum5 = () => {
    var rows = [];
    for (var i = 61; i <= 75; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  winbingo = async () => {
    this.setState({ bingobtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum) && unum.length === 24) {
      const gamedet = {
        gameid: this.props.getbingo.gameid,
        cardid: this.state.ticket[0].cardid,
        winpat: "Bingo",
      };
      console.log(gamedet);
      const { data } = await bingo.gamebingo(gamedet);
      console.log(data);
      try {
        if (data) {
          toast.success(data, {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          await this.setState({ bingobtndisable: true });
        }
      } catch (ex) {
        if (ex.response && ex.response.status === 400) {
          toast.error(ex.response.data, {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      }
    } else {
      await this.setState({ bingobtndisable: this.state.bingobtndisable });
    }
  };
  anydiagonal = async () => {
    this.setState({ diagonalbtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      const di = await WinPatterns.DiagonalPattern(this.state.userPattern);
      if (di && di.length) {
        di.map((n) => {
          this.state.patternmap.push(n);
        });

        const gamedet = {
          gameid: this.props.getbingo.gameid,
          cardid: this.state.ticket[0].cardid,
          winpat: "Diagonal",
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        } catch (ex) {
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        }
        this.setState({ diagonalbtndisable: true });
      } else {
        this.setState({ diagonalbtndisable: false });
      }
    }
  };
  anyrow = async () => {
    this.setState({ rowbtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      const hi = WinPatterns.HorizontalPattern(this.state.userPattern);
      if (hi && hi.length) {
        hi.map((n) => {
          if (!this.state.patternmap.includes(n)) {
            this.state.patternmap.push(n);
          }
        });
        const gamedet = {
          gameid: this.props.getbingo.gameid,
          cardid: this.state.ticket[0].cardid,
          winpat: "Row",
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        } catch (ex) {
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        }
        this.setState({ rowbtndisable: true });
      } else {
        this.setState({ rowbtndisable: false });
      }
    }
  };
  tworows = async () => {
    this.setState({ tworowsbtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      const hi = WinPatterns.tworowsPattern(this.state.userPattern);
      if (hi && hi.length) {
        hi.map((n) => {
          if (!this.state.patternmap.includes(n)) {
            this.state.patternmap.push(n);
          }
        });
        const gamedet = {
          gameid: this.props.getbingo.gameid,
          cardid: this.state.ticket[0].cardid,
          winpat: "Two rows",
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        } catch (ex) {
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        }
        this.setState({ tworowsbtndisable: true });
      } else {
        this.setState({ tworowsbtndisable: false });
      }
    }
  };
  threerows = async () => {
    this.setState({ threerowsbtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      const hi = WinPatterns.threerowsPattern(this.state.userPattern);
      if (hi && hi.length) {
        hi.map((n) => {
          if (!this.state.patternmap.includes(n)) {
            this.state.patternmap.push(n);
          }
        });
        const gamedet = {
          gameid: this.props.getbingo.gameid,
          cardid: this.state.ticket[0].cardid,
          winpat: "Three rows",
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        } catch (ex) {
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        }
        this.setState({ threerowsbtndisable: true });
      } else {
        this.setState({ threerowsbtndisable: false });
      }
    }
  };
  fourrows = async () => {
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      const hi = WinPatterns.fourrowsPattern(this.state.userPattern);
      if (hi && hi.length) {
        hi.map((n) => {
          if (!this.state.patternmap.includes(n)) {
            this.state.patternmap.push(n);
          }
        });
        const gamedet = {
          gameid: this.props.getbingo.gameid,
          cardid: this.state.ticket[0].cardid,
          winpat: "Four rows",
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        } catch (ex) {
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        }
        // this.setState({ fourrowsbtndisable: true });
      } else {
        this.setState({ fourrowsbtndisable: false });
      }
    }
  };
  anycolumn = async () => {
    this.setState({ twocolbtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      const hi = WinPatterns.VerticalPattern(this.state.userPattern);
      console.log(hi);
      if (hi && hi.length) {
        hi.map((n) => {
          if (!this.state.patternmap.includes(n)) {
            this.state.patternmap.push(n);
          }
        });
        const gamedet = {
          gameid: this.props.getbingo.gameid,
          cardid: this.state.ticket[0].cardid,
          winpat: "Column",
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        } catch (ex) {
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
        }
        this.setState({ twocolbtndisable: true });
      } else {
        this.setState({ twocolbtndisable: false });
      }
    }
  };

  render() {
    const { getbingo } = this.props;

    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <section id="add-vertical" />
            </div>
          </div>
        </div>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="title tambola-start">
                      <div className="title-row clearfix">
                        <div className="gamenum">
                          <div className="tgame-no">
                            Game no.
                            {getbingo.gameid}
                          </div>
                        </div>
                        <div className="date-time">
                          <div className="game-details">
                            <div className="clearfix">
                              <div className="take-left">
                                <i className="fa fa-calendar-alt" />
                                <span className="ml-2 current-date">
                                  <Moment format="D MMM YYYY" withTitle />
                                </span>
                              </div>
                              <div className="take-right">
                                <span className="icon-t">
                                  <img src={clock} alt="time" />
                                </span>{" "}
                                <span className=" current-time">
                                  <Moment format="hh:mm A" withTitle />{" "}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {this.state.winner ? (
                      <section
                        id="mid-section"
                        className={this.state.winner ? "bg-bingo" : null}
                      >
                        <div className="container">
                          <div className="row">
                            <div className="col-md-12">
                              <div id="play-tambola">
                                <div className="row pb-5">
                                  <div className="tambola-info col-md-10 pt-5 pb-5">
                                    <p
                                      style={{
                                        fontSize: "40px",
                                        textAlign: "center",
                                        fontWeight: 500,
                                      }}
                                    >
                                      {" "}
                                      Congratulations!!
                                    </p>
                                    <div className="game-startat">
                                      <p>
                                        <span
                                          class="win_text"
                                          style={{ fontSize: "56px" }}
                                        >
                                          {this.state.winner}
                                        </span>
                                        &nbsp;
                                      </p>
                                      Bingo Winner
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    ) : this.state.ticket && this.state.ticket.length ? (
                      <div className="board-border">
                        <div className=" digit_table">
                          <div className=" row ">
                            <div className="">
                              <div className="container ">
                                <div className=" row text-center">
                                  <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="tkt-no ml-1 mt-4">
                                      <div class="tkt-no-round">
                                        {this.state.num.length > 0 ? (
                                          this.state.num.map((num) =>
                                            num >= 1 && num <= 15 ? (
                                              <div class="bingo-num">
                                                <span>B-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 15 && num <= 30) ? (
                                              <div class="bingo-num">
                                                <span>I-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 30 && num <= 45) ? (
                                              <div class="bingo-num">
                                                <span>N-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 46 && num <= 60) ? (
                                              <div class="bingo-num">
                                                <span>G-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 60 && num <= 75) ? (
                                              <div class="bingo-num">
                                                <span>O-{num}</span>
                                              </div>
                                            ) : null
                                          )
                                        ) : (
                                          <div class="bingo-num">
                                            <span>Please wait..</span>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                  {/* <div className="col offset-1">
                                  <div class="tkt-no-round">
                                    <div class="bingo-num">{"B 12"}</div>
                                  </div>
                                  <ReactCountdownClock
                                    seconds={10}
                                    color="#e2bd0b"
                                    weight={10}
                                    // alpha={0.9}
                                    size={40}
                                    onComplete={() => this.nextnum()}
                                  />
                                </div> */}
                                  <div className="col-md-6 mt-3 pr-0 pl-1">
                                    <div className="row">
                                      <div className="digit name-display-box">
                                        B
                                      </div>
                                      {this.selectnum1()}
                                    </div>
                                    <div className="row">
                                      {" "}
                                      <div className="digit name-display-box">
                                        I
                                      </div>
                                      {this.selectnum2()}
                                    </div>
                                    <div className="row">
                                      {" "}
                                      <div className="digit name-display-box">
                                        N
                                      </div>
                                      {this.selectnum3()}
                                    </div>
                                    <div className="row">
                                      {" "}
                                      <div className="digit name-display-box">
                                        G
                                      </div>
                                      {this.selectnum4()}
                                    </div>
                                    <div className="row">
                                      {" "}
                                      <div className="digit name-display-box">
                                        O
                                      </div>
                                      {this.selectnum5()}
                                    </div>
                                  </div>
                                  <div className="col-md-4 p-0 d-none d-md-block">
                                    <div className="row pay-table">
                                      <div className="col-md-4 pl-0 pr-0">
                                        <img
                                          src={require("../../assets/images/d_1.png")}
                                          className="pr-1 mb-1"
                                        />
                                        <span> Claim Diagonal </span>
                                      </div>

                                      <div className="col-md-4 pl-0 pr-0">
                                        <img
                                          src={require("../../assets/images/c_1.png")}
                                          className="pr-1 mb-1"
                                        />
                                        <span> Claim 2nd - Column</span>
                                      </div>

                                      <div className="col-md-4 pl-0 pr-0">
                                        <img
                                          src={require("../../assets/images/r_1.png")}
                                          className="pr-1 mb-1"
                                        />
                                        <span> Claim 1st - Row </span>
                                      </div>

                                      <div className="col-md-4 pl-0 pr-0 mt-2">
                                        <img
                                          src={require("../../assets/images/r_2.png")}
                                          className="pr-1 mb-1"
                                        />
                                        <span>Claim 1,4 - Rows </span>
                                      </div>

                                      <div className="col-md-4 pl-0 pr-0 mt-2">
                                        <img
                                          src={require("../../assets/images/r_3.png")}
                                          className="pr-1 mb-1"
                                        />
                                        <span> Claim 1,3,5 - Rows</span>
                                      </div>

                                      <div className="col-md-4 pl-0 pr-0 mt-2">
                                        <img
                                          src={require("../../assets/images/over.png")}
                                          className="pr-1 mb-1"
                                        />
                                        <span> Bingo </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="row ">
                                  <div className="d-flex ml-2">
                                    <p className="text-white mr-3 mt-3">
                                      Next in
                                    </p>
                                    {this.state.ct ? (
                                      <ReactCountdownClock
                                        seconds={10}
                                        color="#e2bd0b"
                                        weight={10}
                                        alpha={0.9}
                                        size={40}
                                        onComplete={() => this.nextnum()}
                                      />
                                    ) : (
                                      <ReactCountdownClock
                                        seconds={0}
                                        color="#e2bd0b"
                                        weight={10}
                                        alpha={0.9}
                                        size={40}
                                      />
                                    )}

                                    <p className="text-white ml-2 mt-3">Sec</p>
                                  </div>
                                  <div className="row mb-3">
                                    <div class="col-md-8 mt-2">
                                      <div className="row">
                                        {this.state.ticket &&
                                        this.state.ticket.length > 0
                                          ? this.state.ticket.map((card) => {
                                              return (
                                                <div className="col-md-6 m-auto ">
                                                  <table className="bingo_box border-r">
                                                    <thead>
                                                      <th className="digit  mr-2 mt-1 mb-1">
                                                        B
                                                      </th>
                                                      <th className="digit  mr-2 mt-1 mb-1">
                                                        I
                                                      </th>
                                                      <th className="digit  mr-2 mt-1 mb-1">
                                                        N
                                                      </th>
                                                      <th className="digit  mr-2 mt-1 mb-1">
                                                        G
                                                      </th>
                                                      <th className="digit  mr-2 mt-1 mb-1">
                                                        O
                                                      </th>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].b[0]
                                                          }
                                                          id="11"
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              11
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  11
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                        >
                                                          {card.card[0].b[0]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].i[0]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              12
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  12
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="12"
                                                        >
                                                          {card.card[0].i[0]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].n[0]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              13
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  13
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="13"
                                                        >
                                                          {card.card[0].n[0]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].g[0]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              14
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  14
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="14"
                                                        >
                                                          {card.card[0].g[0]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].o[0]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              15
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  15
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="15"
                                                        >
                                                          {card.card[0].o[0]}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td
                                                          onClick={this.idValue}
                                                          id="21"
                                                          data-value={
                                                            card.card[0].b[1]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              21
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  21
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                        >
                                                          {card.card[0].b[1]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].i[1]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              22
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  22
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="22"
                                                        >
                                                          {" "}
                                                          {card.card[0].i[1]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].n[1]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              23
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  23
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="23"
                                                        >
                                                          {card.card[0].n[1]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].g[1]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              24
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  24
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="24"
                                                        >
                                                          {card.card[0].g[1]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].o[1]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              25
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  25
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="25"
                                                        >
                                                          {card.card[0].o[1]}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].b[2]
                                                          }
                                                          id="31"
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              31
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  31
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                        >
                                                          {card.card[0].b[2]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].i[2]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              32
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  32
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="32"
                                                        >
                                                          {" "}
                                                          {card.card[0].i[2]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value="33"
                                                          className="digit row-box_2"
                                                          // className={
                                                          //   this.state
                                                          //     .userPattern &&
                                                          //   this.state.userPattern.includes(
                                                          //     33
                                                          //   )
                                                          //     ? this.state.patternmap.includes(
                                                          //         33
                                                          //       )
                                                          //       ? "pat-active "
                                                          //       : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                          //     : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          // }
                                                          id="33"
                                                        >
                                                          <img
                                                            src={require("../../assets/images/star.png")}
                                                            className="star"
                                                          />
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].g[2]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              34
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  34
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="34"
                                                        >
                                                          {card.card[0].g[2]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].o[2]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              35
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  35
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="35"
                                                        >
                                                          {card.card[0].o[2]}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].b[3]
                                                          }
                                                          id="41"
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              41
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  41
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                        >
                                                          {card.card[0].b[3]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].i[3]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              42
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  42
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="42"
                                                        >
                                                          {" "}
                                                          {card.card[0].i[3]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].n[2]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              43
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  43
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="43"
                                                        >
                                                          {card.card[0].n[2]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].g[3]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              44
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  44
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="44"
                                                        >
                                                          {card.card[0].g[3]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].o[3]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              45
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  45
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="45"
                                                        >
                                                          {card.card[0].o[3]}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].b[4]
                                                          }
                                                          id="51"
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              51
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  51
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                        >
                                                          {card.card[0].b[4]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].i[4]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              52
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  52
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="52"
                                                        >
                                                          {card.card[0].i[4]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].n[3]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              53
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  53
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="53"
                                                        >
                                                          {card.card[0].n[3]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].g[4]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              54
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  54
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="54"
                                                        >
                                                          {card.card[0].g[4]}
                                                        </td>

                                                        <td
                                                          onClick={this.idValue}
                                                          data-value={
                                                            card.card[0].o[4]
                                                          }
                                                          className={
                                                            this.state
                                                              .userPattern &&
                                                            this.state.userPattern.includes(
                                                              55
                                                            )
                                                              ? this.state.patternmap.includes(
                                                                  55
                                                                )
                                                                ? "pat-active "
                                                                : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                              : "digit row-box_2 mr-2 mt-1 mb-1"
                                                          }
                                                          id="55"
                                                        >
                                                          {card.card[0].o[4]}
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                  <div className="tckt_id">
                                                    {" "}
                                                    #{card.cardid}
                                                  </div>
                                                </div>
                                              );
                                            })
                                          : null}
                                        {/* <div className="col-md-4 m-auto">
                                      <table className="bingo_box">
                                        <thead>
                                          <th className="digit  mr-2 mt-1 mb-1">
                                            B
                                          </th>
                                          <th className="digit  mr-2 mt-1 mb-1">
                                            I
                                          </th>
                                          <th className="digit  mr-2 mt-1 mb-1">
                                            N
                                          </th>
                                          <th className="digit  mr-2 mt-1 mb-1">
                                            G
                                          </th>
                                          <th className="digit  mr-2 mt-1 mb-1">
                                            O
                                          </th>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td
                                              onClick={this.idValue}
                                              id="11"
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                            >
                                              {card.card[0].b[0]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="21"
                                            >
                                              {numbers.i[0]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="31"
                                            >
                                              {numbers.n[0]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="41"
                                            >
                                              {numbers.g[0]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="51"
                                            >
                                              {numbers.o[0]}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              onClick={this.idValue}
                                              id="21"
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                            >
                                              {numbers.b[1]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="22"
                                            >
                                              {" "}
                                              {numbers.i[1]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="23"
                                            >
                                              {numbers.n[1]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="24"
                                            >
                                              {numbers.g[1]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="25"
                                            >
                                              {numbers.o[1]}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              onClick={this.idValue}
                                              id="31"
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                            >
                                              {numbers.b[2]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="32"
                                            >
                                              {" "}
                                              {numbers.i[2]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="33"
                                            >
                                              <img
                                                src={require("../../assets/images/star.png")}
                                                className="star"
                                              />
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="34"
                                            >
                                              {numbers.g[2]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="35"
                                            >
                                              {numbers.o[2]}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              onClick={this.idValue}
                                              id="41"
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                            >
                                              {numbers.b[3]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="42"
                                            >
                                              {" "}
                                              {numbers.i[3]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="43"
                                            >
                                              {numbers.n[2]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="44"
                                            >
                                              {numbers.g[3]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="45"
                                            >
                                              {numbers.o[3]}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              onClick={this.idValue}
                                              id="51"
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                            >
                                              {numbers.b[4]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="52"
                                            >
                                              {numbers.i[4]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="53"
                                            >
                                              {numbers.n[3]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="54"
                                            >
                                              {numbers.g[4]}
                                            </td>

                                            <td
                                              onClick={this.idValue}
                                              className="digit row-box_2 mr-2 mt-1 mb-1"
                                              id="55"
                                            >
                                              {numbers.o[4]}
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>*/}
                                      </div>
                                    </div>

                                    <div className="col-md-4">
                                      <div className="game-btn">
                                        <button
                                          type="button"
                                          disabled={
                                            this.state.diagonalbtndisable
                                          }
                                          class="btn btn-lines btn-block"
                                          onClick={this.anydiagonal}
                                        >
                                          {this.state.claimdiagonal}
                                        </button>
                                      </div>{" "}
                                      <div className="game-btn">
                                        {" "}
                                        <button
                                          disabled={this.state.twocolbtndisable}
                                          class="btn btn btn-lines btn-block"
                                          onClick={this.anycolumn}
                                        >
                                          {this.state.claim2ndcol}
                                        </button>
                                      </div>
                                      <div className="game-btn">
                                        {" "}
                                        <button
                                          disabled={this.state.rowbtndisable}
                                          class="btn btn btn-lines btn-block"
                                          onClick={this.anyrow}
                                        >
                                          {this.state.claim1strow}
                                        </button>
                                      </div>{" "}
                                      <div className="game-btn">
                                        {" "}
                                        <button
                                          disabled={
                                            this.state.tworowsbtndisable
                                          }
                                          class="btn btn btn-lines btn-block"
                                          onClick={this.tworows}
                                        >
                                          {this.state.claim2rows}
                                        </button>
                                      </div>{" "}
                                      <div className="game-btn">
                                        {" "}
                                        <button
                                          disabled={
                                            this.state.threerowsbtndisable
                                          }
                                          class="btn btn btn-lines btn-block"
                                          onClick={this.threerows}
                                        >
                                          {this.state.claim3rows}
                                        </button>
                                      </div>{" "}
                                      {/* <div className="game-btn">
                                        {" "}
                                        <button
                                          disabled={
                                            this.state.fourrowsbtndisable
                                          }
                                          class="btn btn btn-lines btn-block"
                                          onClick={this.fourrows}
                                        >
                                          Claim 1,2,3,4 - Rows
                                        </button>
                                      </div>{" "} */}
                                      <div className="game-btn">
                                        {" "}
                                        <button
                                          disabled={this.state.bingobtndisable}
                                          class="btn btn btn-start btn-block"
                                          onClick={this.winbingo}
                                        >
                                          Bingo
                                        </button>
                                      </div>{" "}
                                    </div>
                                  </div>

                                  {/* <div className="col-md-4"> 
                                  <table className="bingo_box">
                                    <thead>
                                      <th className="digit  mr-2 mt-1 mb-1">
                                        B
                                      </th>
                                      <th className="digit  mr-2 mt-1 mb-1">
                                        I
                                      </th>
                                      <th className="digit  mr-2 mt-1 mb-1">
                                        N
                                      </th>
                                      <th className="digit  mr-2 mt-1 mb-1">
                                        G
                                      </th>
                                      <th className="digit  mr-2 mt-1 mb-1">
                                        O
                                      </th>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td
                                          onClick={this.idValue}
                                          id="11"
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                        >
                                          {numbers.b[0]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="21"
                                        >
                                          {numbers.i[0]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="31"
                                        >
                                          {numbers.n[0]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="41"
                                        >
                                          {numbers.g[0]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="51"
                                        >
                                          {numbers.o[0]}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td
                                          onClick={this.idValue}
                                          id="21"
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                        >
                                          {numbers.b[1]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="22"
                                        >
                                          {" "}
                                          {numbers.i[1]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="23"
                                        >
                                          {numbers.n[1]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="24"
                                        >
                                          {numbers.g[1]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="25"
                                        >
                                          {numbers.o[1]}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td
                                          onClick={this.idValue}
                                          id="31"
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                        >
                                          {numbers.b[2]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="32"
                                        >
                                          {" "}
                                          {numbers.i[2]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="33"
                                        >
                                          <img
                                            src={require("../../assets/images/star.png")}
                                            className="star"
                                          />
                                        </td>
                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="34"
                                        >
                                          {numbers.g[2]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="35"
                                        >
                                          {numbers.o[2]}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td
                                          onClick={this.idValue}
                                          id="41"
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                        >
                                          {numbers.b[3]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="42"
                                        >
                                          {" "}
                                          {numbers.i[3]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="43"
                                        >
                                          {numbers.n[2]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="44"
                                        >
                                          {numbers.g[3]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="45"
                                        >
                                          {numbers.o[3]}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td
                                          onClick={this.idValue}
                                          id="51"
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                        >
                                          {numbers.b[4]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="52"
                                        >
                                          {numbers.i[4]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="53"
                                        >
                                          {numbers.n[3]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="54"
                                        >
                                          {numbers.g[4]}
                                        </td>

                                        <td
                                          onClick={this.idValue}
                                          className="digit row-box_2 mr-2 mt-1 mb-1"
                                          id="55"
                                        >
                                          {numbers.o[4]}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>*/}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div className="offset-md-3">
                        <img src={require("../../assets/images/loader.gif")} />
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getbingo: state.getbingo,
  };
};

export default connect(mapStateToProps)(Bingo);
