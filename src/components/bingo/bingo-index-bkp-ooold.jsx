import React, { Component } from "react";
import Moment from "react-moment";
import { connect } from "react-redux";
import moment from "moment";
import clock from "../../assets/images/clock1.png";
import { toast } from "react-toastify";
import get_bingogame from "../../redux/actions/getbingoActions";
import auth from "../../services/authService";
import bingo from "../../services/bingoservice";
import get_bingotick from "../../redux/actions/bingotickAction";

class Index extends Component {
  state = {
    selectedCards: null,
    card: 1,
    disable: false,
    games: [],
  };
  componentDidMount = async () => {
    const { getbingo } = await this.props;
    if (!getbingo) {
      await get_bingogame();
      await get_bingotick();
    }
    const user = auth.getCurrentUser();
  };
  openBingo = async () => {
    toast.configure();
    if (this.state.card === 1) {
      const user = await auth.getCurrentUser();
      const obj = {
        gameid: this.props.getbingo.gameid,
        totaltick: this.state.card.toString(),
        user: user.email,
      };
      try {
        const data = await bingo.sendgame(obj);
        if (data) {
          toast.success("Bought the ticket successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
        this.setState({ disable: true });
      } catch (ex) {
        if (ex.response && ex.response.status === 400) {
          toast.error("Please Buy Ticket Before Game Resumes", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
        }
      }
    }
  };
  decrement = () => {
    if (!this.state.card <= 0) {
      this.setState({ card: this.state.card - 1 });
    }
  };
  increment = () => {
    toast.configure();
    if (this.state.card <= 3) {
      this.setState({ card: this.state.card + 1 });
    } else {
      toast.error("You Should select maximum three only", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
    }
  };
  resumeBingo = async () => {
    toast.configure();
    const btick = await this.props.bingotick;
    setTimeout(async () => {
      if (btick && btick.length >= 1) {
        await this.props.history.push("/bingo");
      } else {
        toast.error("Please Wait For Next Game", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      }
    }, 300);
  };
  render() {
    const { getbingo } = this.props;
    // console.log(getbingo);
    return (
      <React.Fragment>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="title tambola-start">
                      <div className="title-row clearfix">
                        <div className="gamenum">
                          <div className="tgame-no">
                            Game no.{getbingo.gameid}
                          </div>
                        </div>
                        <div className="date-time">
                          <div className="game-details">
                            <div className="clearfix">
                              <div className="take-left">
                                <i className="fa fa-calendar-alt" />
                                <span className="ml-2 current-date">
                                  <Moment format="D MMM YYYY" withTitle />
                                </span>
                              </div>
                              <div className="take-right">
                                <span className="icon-t">
                                  <img src={clock} alt="time" />
                                </span>{" "}
                                <span className=" current-time">
                                  <Moment format="hh:mm A" withTitle />{" "}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tambola-start-bottom mt-0">
                      <div className="row">
                        {getbingo.status === "pending" ? (
                          <div className="tambola-info">
                            <h1>Game Starts At</h1>
                            <div className="game-startat">
                              <p>
                                <span style={{ fontSize: "50px" }}>
                                  {/* <Moment format="D MMM YYYY hh:mm A" withTitle />{" "} */}
                                  {moment(getbingo.date).format(
                                    "D MMM YYYY hh:mm A"
                                  )}
                                </span>
                                &nbsp;
                              </p>
                            </div>
                          </div>
                        ) : (
                          <div className="tambola-info col-md-12 ">
                            <h1>Game Resumes At</h1>
                            <div
                              className="game-startat offset-2 d-flex
                            "
                            >
                              <p>
                                <span style={{ fontSize: "50px" }}>
                                  {/* <Moment format="D MMM YYYY hh:mm A" withTitle />{" "} */}
                                  {moment(getbingo.date).format(
                                    "D MMM YYYY hh:mm A"
                                  )}
                                </span>
                                &nbsp;
                              </p>
                              {getbingo ? (
                                <div className=" ">
                                  <button
                                    onClick={this.resumeBingo}
                                    type="button"
                                    className="
                                  btn
                                  btn-danger
                                  bg-orange ml-5"
                                  >
                                    Resume
                                  </button>{" "}
                                </div>
                              ) : null}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>

                    <div className="board-border1">
                      <div className=" digit_table">
                        <div className="card-bingo">
                          {/* <h1 className="text-center ">SELECT CARDS</h1> */}
                          <div className="">
                            <div className="col-md-6 nums_card-1 pt-0 offset-md-3">
                              {/* <div className="card-header title">
                                <h5>NUM CARDS</h5>
                              </div> */}
                              {/* <div className="row m-auto nums_block">
                                {this.state.card <= 0 ? (
                                  <div className="col-md-3 ">
                                    <div
                                      className=" nums_block_1 disable"
                                      // onClick={this.decrement}
                                      style={{ cursor: "not-allowed" }}
                                    >
                                      <i
                                        class="fa fa-minus"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                  </div>
                                ) : (
                                  <div className="col-md-3 ">
                                    <div
                                      className=" nums_block_1 disable"
                                      // onClick={this.decrement}
                                      style={{ cursor: "not-allowed" }}
                                    >
                                      <i
                                        class="fa fa-minus"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                  </div>
                                )}

                                <div className="col-md-6 p-0">
                                  <div className=" nums_block_2 ">
                                    <div className="quantity">
                                      {" "}
                                      {this.state.card}{" "}
                                    </div>
                                  </div>
                                </div>
                                {this.state.card >= 3 ? (
                                  <div className="col-md-3 ">
                                    <div
                                      className=" nums_block_3"
                                      style={{ cursor: "not-allowed" }}
                                      // onClick={this.increment}
                                    >
                                      <i
                                        class="fa fa-plus"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                  </div>
                                ) : (
                                  <div className="col-md-3 ">
                                    <div
                                      className=" nums_block_3"
                                      // onClick={this.increment}
                                      style={{ cursor: "not-allowed" }}
                                    >
                                      <i
                                        class="fa fa-plus"
                                        aria-hidden="true"
                                      ></i>
                                    </div>
                                  </div>
                                )}
                              </div> */}
                              {/* {this.state.card  } */}
                              <div className="card-header btn-title">
                                <div className="row m-auto nums_block">
                                  {this.state.card <= 0 ? (
                                    <div className="col-md-3 ">
                                      <div
                                        className=" nums_block_1 disable"
                                        // onClick={this.decrement}
                                        style={{ cursor: "not-allowed" }}
                                      >
                                        <i
                                          class="fa fa-minus"
                                          aria-hidden="true"
                                        ></i>
                                      </div>
                                    </div>
                                  ) : (
                                    <div className="col-md-3 ">
                                      <div
                                        className=" nums_block_1 disable"
                                        // onClick={this.decrement}
                                        style={{ cursor: "not-allowed" }}
                                      >
                                        <i
                                          class="fa fa-minus"
                                          aria-hidden="true"
                                        ></i>
                                      </div>
                                    </div>
                                  )}

                                  <div className="col-md-6 p-0">
                                    <div className=" nums_block_2 ">
                                      <div className="quantity">
                                        {" "}
                                        {this.state.card}{" "}
                                      </div>
                                    </div>
                                  </div>
                                  {this.state.card >= 3 ? (
                                    <div className="col-md-3 ">
                                      <div
                                        className=" nums_block_3"
                                        style={{ cursor: "not-allowed" }}
                                        // onClick={this.increment}
                                      >
                                        <i
                                          class="fa fa-plus"
                                          aria-hidden="true"
                                        ></i>
                                      </div>
                                    </div>
                                  ) : (
                                    <div className="col-md-3 ">
                                      <div
                                        className=" nums_block_3"
                                        // onClick={this.increment}
                                        style={{ cursor: "not-allowed" }}
                                      >
                                        <i
                                          class="fa fa-plus"
                                          aria-hidden="true"
                                        ></i>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>
                            </div>

                            <div className="col-md-6 offset-md-3">
                              <div className="paytable pt-2 pb-3 mt-0">
                                <p>PATTERNS</p>
                                <div className="">
                                  <img
                                    src={require("../../assets/images/d_1.png")}
                                    className="pr-1 mb-1"
                                  />

                                  <img
                                    src={require("../../assets/images/c_1.png")}
                                    className="pr-1 mb-1"
                                  />

                                  <img
                                    src={require("../../assets/images/r_1.png")}
                                    className="pr-1 mb-1"
                                  />
                                  <br />
                                  <img
                                    src={require("../../assets/images/r_2.png")}
                                    className="pr-1 mb-1"
                                  />
                                  <img
                                    src={require("../../assets/images/r_3.png")}
                                    className="pr-1 mb-1"
                                  />
                                  <img
                                    src={require("../../assets/images/r_4.png")}
                                    className="pr-1 mb-1"
                                  />

                                  <br />
                                  <img
                                    src={require("../../assets/images/over.png")}
                                    className="pr-1 mb-1"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <div class="row">
              <div class="col-md-12">
                <div id="play-tambola">
                  <div class="tambola-result">
                    <div class="tambolawinners-details">
                      <div class="tambola-winners-title">
                        <h1>Last 5 Games</h1>
                      </div>
                      <div class="last-gamewinners">
                        <table class="table table-striped tambola-winners-list">
                          <thead>
                            <tr>
                              <th style={{ textAlign: "center" }} width="10%">
                                S.No
                              </th>
                              <th style={{ textAlign: "center" }} width="23%">
                                Game Id
                              </th>
                              <th style={{ textAlign: "center" }} width="25%">
                                Your Tickets
                              </th>
                              <th style={{ textAlign: "center" }} width="25%">
                                Game Tickets
                              </th>
                              <th
                                style={{ textAlign: "center" }}
                                width="19%"
                                class="hidden_xs"
                              >
                                Status
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.games.length > 0
                              ? this.state.games.map((item, i) => (
                                  <tr>
                                    <td style={{ textAlign: "center" }}>
                                      {i + 1}
                                    </td>
                                    <td style={{ textAlign: "center" }}>
                                      {item.gameid}
                                    </td>
                                    <td style={{ textAlign: "center" }}>
                                      {item.tickets}{" "}
                                      {item.tickets > 0 &&
                                      item.status === "Pending" ? (
                                        <button
                                          class="btn-sm btn-danger"
                                          onClick={() =>
                                            this.cancelticket(item.gameid)
                                          }
                                          disabled={this.state.cancelbutton}
                                        >
                                          Cancel Tickets
                                        </button>
                                      ) : null}
                                    </td>
                                    <td style={{ textAlign: "center" }}>
                                      {item.gametics}
                                    </td>
                                    <td>
                                      {item.status === "Resume" ? (
                                        <div className="resume">
                                          <button
                                            class="btn-sm btn-success"
                                            onClick={() =>
                                              this.resumeGame(item.gameid)
                                            }
                                          >
                                            {" "}
                                            Resume
                                          </button>
                                        </div>
                                      ) : (
                                        item.status
                                      )}
                                    </td>
                                  </tr>
                                ))
                              : null}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>*/}
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getbingo: state.getbingo,
    bingotick: state.bingotick,
  };
};

export default connect(mapStateToProps)(Index);
