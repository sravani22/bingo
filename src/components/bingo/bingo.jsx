import React, { Component } from "react";
import { connect } from "react-redux";

import ReactCountdownClock from "react-countdown-clock";
import Moment from "react-moment";
import { toast } from "react-toastify";
import clock from "../../assets/images/clock1.png";
import get_bingogame from "./../../redux/actions/getbingoActions";
import bingo from "./../../services/bingoservice";
import { WinPatterns } from "./winpatterns";
import get_bingotick from "../../redux/actions/bingotickAction";
import Loader from "react-loader-spinner";

class Bingo extends Component {
  state = {
    gamenum: [],
    num: [],
    ct: true,
    userPattern: [],
    ticket: [],
    usernums: [],
    winner: "",
    patt: "diagonal",
    claimdiagonal: "Claim Diagonal",
    claim1strow: "Claim 1st Row",
    claim2ndcol: "Claim 2nd Column",
    claim2rows: "Claim 1,4 - Rows",
    claim3rows: "Claim 1,3,5 - Rows",
    bingobtndisable: false,
    isbingo: false,
    patternmap: [],
    claimcard: "",
    arr: [],
    selectid: [],
    loader: false,
  };

  componentDidMount = async () => {
    const { getbingo } = this.props;
    if (!getbingo) {
      await get_bingogame();
      await get_bingotick();
    }
    const { data } = await bingo.gettickets();
    try {
      if (data.length < 1) {
        this.props.history.push("/index");
      }
      await this.setState({ ticket: data });
    } catch (error) {}
    // const user = auth.getCurrentUser();
  };
  idValue = async (carr, e, vas) => {
    if (this.state.gamenum.includes(parseFloat(e.target.dataset.value))) {
      var tikid = e.target.id;
      var cardy = carr.cardid;
      var dval = e.target.dataset.value;
      await this.setState({ loader: true });
      if (this.state.arr.some((win) => win.cardid === cardy)) {
        this.state.arr.some((win) => {
          if (win.cardid === cardy && !win.id.includes(parseFloat(tikid))) {
            win.id.push(parseFloat(tikid));
          }
        });
      } else {
        this.state.arr.push({
          cardid: cardy,
          id: [parseFloat(tikid)],
        });
      }
      await this.setState({ loader: false });
      if (
        this.state.gamenum.includes(parseFloat(dval)) &&
        !this.state.usernums.includes(parseFloat(dval))
      ) {
        const selectnums = this.state.usernums;
        selectnums.push(parseFloat(dval));
        await this.setState({
          usernums: selectnums,
        });
      }
    }
  };
  nextnum = async () => {
    await this.setState({ ct: false });
    var gameid = await this.props.location.state.game;
    console.log(gameid.game.gameid);
    const gamenum = { gameno: gameid.game.gameid };
    const { data } = await bingo.drawnumbers(gamenum);
    if (data.winners.some((win) => win.winpat === gameid.game.pattern)) {
      await get_bingogame();
      data.winners.map(async (win) => {
        if (win.winpat === gameid.game.pattern) {
          await this.setState({ winner: data.winners, bingobtndisable: true });
          setTimeout(() => {
            var msg = new SpeechSynthesisUtterance();
            msg.text = "Congratulations to the bingo winner";
            msg.rate = 0.8;
            msg.lang = "tl-tgl";
            // var voices = window.speechSynthesis.getVoices();
            window.speechSynthesis.speak(msg);
            this.props.history.push({ pathname: "/index" });
          }, 9000);
        }
      });
    }
    await this.setState({ ct: true });
    await this.setState({
      num: [data.drawnum.slice(-1)[0]],
      gamenum: data.drawnum,
    });
    if (this.state.num >= 1 && this.state.num <= 15) {
      var msg = new SpeechSynthesisUtterance();
      msg.text = "b" + this.state.num;
      msg.rate = 0.8;
      msg.lang = "tl-tgl";
      // var voices = window.speechSynthesis.getVoices();
      window.speechSynthesis.speak(msg);
      this.selectnum1();
    } else if (this.state.num > 15 && this.state.num <= 30) {
      var msg = new SpeechSynthesisUtterance();
      msg.text = "i" + this.state.num;
      msg.rate = 0.8;
      msg.lang = "tl-tgl";
      // var voices = window.speechSynthesis.getVoices();
      window.speechSynthesis.speak(msg);
      this.selectnum2();
    } else if (this.state.num > 31 && this.state.num <= 45) {
      var msg = new SpeechSynthesisUtterance();
      msg.text = "n" + this.state.num;
      msg.rate = 0.8;
      msg.lang = "tl-tgl";
      // var voices = window.speechSynthesis.getVoices();
      window.speechSynthesis.speak(msg);
      this.selectnum3();
    } else if (this.state.num > 46 && this.state.num <= 60) {
      var msg = new SpeechSynthesisUtterance();
      msg.text = "g" + this.state.num;
      msg.rate = 0.8;
      msg.lang = "tl-tgl";
      // var voices = window.speechSynthesis.getVoices();
      window.speechSynthesis.speak(msg);
      this.selectnum4();
    } else if (this.state.num > 61 && this.state.num <= 75) {
      var msg = new SpeechSynthesisUtterance();
      msg.text = "o" + this.state.num;
      msg.rate = 0.8;
      msg.lang = "tl-tgl";
      // var voices = window.speechSynthesis.getVoices();
      window.speechSynthesis.speak(msg);
      this.selectnum5();
    }
  };
  selectnum1 = () => {
    var rows = [];
    for (var i = 1; i <= 15; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum2 = () => {
    var rows = [];
    for (var i = 16; i <= 30; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum3 = () => {
    var rows = [];
    for (var i = 31; i <= 45; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum4 = () => {
    var rows = [];
    for (var i = 46; i <= 60; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectnum5 = () => {
    var rows = [];
    for (var i = 61; i <= 75; i++) {
      rows.push(
        <div
          className={
            this.state.gamenum.includes(i)
              ? "digit num-active "
              : "digit num-display-box"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };

  winbingo = async (card, pat) => {
    await this.setState({ loader: true });
    const cardid = card.cardid;
    this.state.arr.filter(async (win) => {
      if (win.cardid === cardid) {
        var nn = win.id;
        //await this.state.userPattern.push(nn);
        await this.setState({ userPattern: nn });
      }
    });
    this.setState({ claimcard: card.cardid });
    this.setState({ bingobtndisable: true });
    toast.configure();
    const gnum = this.state.gamenum;
    const unum = this.state.usernums;
    var gameid = await this.props.location.state.game;
    let checker = (unum, gnum) => unum.every((v) => gnum.includes(v));
    if (checker(unum, gnum)) {
      var patt = gameid.game.pattern;
      if (pat === gameid.game.pattern) {
        var di = await WinPatterns[patt](this.state.userPattern);
      }
      if (di && di.length) {
        di.map((n) => {
          this.state.patternmap.push(n);
        });
        const gamedet = {
          gameid: gameid.game.gameid,
          cardid: card.cardid,
          winpat: gameid.game.pattern,
          usernum: this.state.usernums,
        };
        try {
          const { data } = await bingo.gamebingo(gamedet);
          if (data) {
            await this.setState({ loader: false });
            toast.success(data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
            await this.setState({ bingobtndisable: false });
          }
        } catch (ex) {
          await this.setState({ loader: false });
          if (ex.response && ex.response.status === 400) {
            toast.error(ex.response.data, {
              position: toast.POSITION.BOTTOM_LEFT,
            });
          }
          await this.setState({ bingobtndisable: false });
        }
      } else {
        await this.setState({ loader: false });
        await this.setState({ bingobtndisable: false });
      }
    } else {
      await this.setState({ loader: false });
      await this.setState({ bingobtndisable: false });
    }
  };

  render() {
    const getbingo = this.props.location.state.game;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <section id="add-vertical" />
            </div>
          </div>
        </div>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="title tambola-start">
                      <div className="title-row clearfix">
                        <div className="gamenum">
                          <div className="tgame-no">
                            Game no.
                            {getbingo ? getbingo.game.gameid : null}
                          </div>
                        </div>
                        <div className="date-time">
                          <div className="game-details">
                            <div className="clearfix">
                              <div className="take-left">
                                <i className="fa fa-calendar-alt" />
                                <span className="ml-2 current-date">
                                  <Moment format="D MMM YYYY" withTitle />
                                </span>
                              </div>
                              <div className="take-right">
                                <span className="icon-t">
                                  <img src={clock} alt="time" />
                                </span>
                                <span className=" current-time">
                                  <Moment format="hh:mm A" withTitle />
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {this.state.winner.length ? (
                      <section
                        id="mid-section"
                        className={this.state.winner ? "bg-bingo" : null}
                      >
                        <div className="container">
                          <div className="row">
                            <div className="col-md-12">
                              <div id="play-tambola">
                                <div className="row pb-5">
                                  <div className="tambola-info col-md-10 pt-5 pb-5">
                                    <p
                                      style={{
                                        fontSize: "40px",
                                        textAlign: "center",
                                        fontWeight: 500,
                                      }}
                                    >
                                      Congratulations!!
                                    </p>
                                    n{" "}
                                    <div className="game-startat">
                                      <p>
                                        {this.state.winner.map((win) => (
                                          <span
                                            className="win_text"
                                            style={{ fontSize: "56px" }}
                                          >
                                            {win.name}
                                            <br />
                                          </span>
                                        ))}
                                        <br />

                                        {this.state.winner.length === 1
                                          ? "Winner"
                                          : "Winners"}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    ) : this.state.ticket && this.state.ticket.length ? (
                      <div className="board-border">
                        <div className=" w-100">
                          <div className=" row ">
                            <div className="w-100">
                              <div className=" col-md-11  text-center">
                                <div className=" row ">
                                  <div className="col-xs-3 col-sm-3 col-md-3">
                                    <div className="tkt-no">
                                      <div className="tkt-no-round">
                                        {this.state.num.length > 0 ? (
                                          this.state.num.map((num) =>
                                            num >= 1 && num <= 15 ? (
                                              <div class="bingo-num m-auto">
                                                <span>B-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 15 && num <= 30) ? (
                                              <div class="bingo-num m-auto">
                                                <span>I-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 30 && num <= 45) ? (
                                              <div class="bingo-num m-auto">
                                                <span>N-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 46 && num <= 60) ? (
                                              <div class="bingo-num m-auto">
                                                <span>G-{num}</span>
                                              </div>
                                            ) : null ||
                                              (num > 60 && num <= 75) ? (
                                              <div class="bingo-num m-auto">
                                                <span>O-{num}</span>
                                              </div>
                                            ) : null
                                          )
                                        ) : (
                                          <div class="bingo-num m-auto">
                                            <span>wait..</span>
                                          </div>
                                        )}
                                      </div>
                                    </div>

                                    <div className="d-flex offset-md-1">
                                      <p className="text-white mr-3 mt-3">
                                        Next in
                                      </p>
                                      {this.state.ct ? (
                                        <ReactCountdownClock
                                          seconds={10}
                                          color="#e2bd0b"
                                          weight={10}
                                          alpha={0.9}
                                          size={40}
                                          onComplete={() => this.nextnum()}
                                        />
                                      ) : (
                                        <ReactCountdownClock
                                          seconds={0}
                                          color="#e2bd0b"
                                          weight={10}
                                          alpha={0.9}
                                          size={40}
                                        />
                                      )}
                                      <p className="text-white ml-2 mt-3">
                                        Sec
                                      </p>
                                    </div>
                                  </div>

                                  <div className="col-md-7  mt-3 pr-0 pl-1">
                                    <div className="row">
                                      <div className="digit name-display-box">
                                        B
                                      </div>
                                      {this.selectnum1()}
                                    </div>
                                    <div className="row">
                                      <div className="digit name-display-box">
                                        I
                                      </div>
                                      {this.selectnum2()}
                                    </div>
                                    <div className="row">
                                      <div className="digit name-display-box">
                                        N
                                      </div>
                                      {this.selectnum3()}
                                    </div>
                                    <div className="row">
                                      <div className="digit name-display-box">
                                        G
                                      </div>
                                      {this.selectnum4()}
                                    </div>
                                    <div className="row">
                                      <div className="digit name-display-box">
                                        O
                                      </div>
                                      {this.selectnum5()}
                                    </div>
                                  </div>
                                  <div className="col-md-2 pr-0 pl-1">
                                    <span class="text-white">PATTERN</span>
                                    {getbingo ? (
                                      <img
                                        src={require(`../../assets/images/${getbingo.game.pattern}.png`)}
                                        alt="img"
                                        class="w-100"
                                      />
                                    ) : (
                                      <div className="pt-4 text-white">
                                        Please Wait..
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                              <div className="container ">
                                {!this.state.loader ? (
                                  <div className="row no-gutters  mt-2">
                                    <div className="col-md-12 mt-2">
                                      <div className="row m-auto">
                                        {this.state.ticket &&
                                        this.state.ticket.length > 0
                                          ? this.state.ticket[0].carddetails.map(
                                              (card) => {
                                                return (
                                                  <div className="col-md-3 m-auto mb-4">
                                                    <table className="bingo_box border-r">
                                                      <thead>
                                                        <th className="digit  mr-2 mt-1 mb-1">
                                                          B
                                                        </th>
                                                        <th className="digit  mr-2 mt-1 mb-1">
                                                          I
                                                        </th>
                                                        <th className="digit  mr-2 mt-1 mb-1">
                                                          N
                                                        </th>
                                                        <th className="digit  mr-2 mt-1 mb-1">
                                                          G
                                                        </th>
                                                        <th className="digit  mr-2 mt-1 mb-1">
                                                          O
                                                        </th>
                                                      </thead>

                                                      <tbody>
                                                        <tr>
                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].b[0]
                                                            }
                                                            id="11"
                                                            className={
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    11
                                                                  )
                                                              )
                                                                ? // this.state
                                                                  //     .claimcard ===
                                                                  //     card.cardid &&
                                                                  //   this.state.patternmap.includes(
                                                                  //     11
                                                                  //   )
                                                                  //   ? "pat-active "
                                                                  // :
                                                                  "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                          >
                                                            {card.card[0].b[0]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].i[0]
                                                            }
                                                            className={
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    12
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    12
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="12"
                                                          >
                                                            {card.card[0].i[0]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].n[0]
                                                            }
                                                            className={
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    13
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    13
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="13"
                                                          >
                                                            {card.card[0].n[0]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].g[0]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .g[0]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    14
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    14
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="14"
                                                          >
                                                            {card.card[0].g[0]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].o[0]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .o[0]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    15
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    15
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="15"
                                                          >
                                                            {card.card[0].o[0]}
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            id="21"
                                                            data-value={
                                                              card.card[0].b[1]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .b[1]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    21
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    21
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                          >
                                                            {card.card[0].b[1]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].i[1]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .i[1]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    22
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    22
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="22"
                                                          >
                                                            {card.card[0].i[1]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].n[1]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .n[1]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    23
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    23
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="23"
                                                          >
                                                            {card.card[0].n[1]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].g[1]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .g[1]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    24
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    24
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="24"
                                                          >
                                                            {card.card[0].g[1]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].o[1]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .o[1]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    25
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    25
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="25"
                                                          >
                                                            {card.card[0].o[1]}
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].b[2]
                                                            }
                                                            id="31"
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .b[2]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    31
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    31
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                          >
                                                            {card.card[0].b[2]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].i[2]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .i[2]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    32
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    32
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="32"
                                                          >
                                                            {" "}
                                                            {card.card[0].i[2]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value="33"
                                                            className="digit row-box_2"
                                                            id="33"
                                                          >
                                                            <img
                                                              src={require("../../assets/images/star.png")}
                                                              className="star"
                                                              alt="img"
                                                            />
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].g[2]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .g[2]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    34
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    34
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="34"
                                                          >
                                                            {card.card[0].g[2]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].o[2]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .o[2]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    35
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    35
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="35"
                                                          >
                                                            {card.card[0].o[2]}
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].b[3]
                                                            }
                                                            id="41"
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .b[3]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    41
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    41
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                          >
                                                            {card.card[0].b[3]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].i[3]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .i[3]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    42
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    42
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="42"
                                                          >
                                                            {" "}
                                                            {card.card[0].i[3]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].n[2]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .n[2]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    43
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    43
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="43"
                                                          >
                                                            {card.card[0].n[2]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].g[3]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .g[3]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    44
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    44
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="44"
                                                          >
                                                            {card.card[0].g[3]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].o[3]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .o[3]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    45
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    45
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="45"
                                                          >
                                                            {card.card[0].o[3]}
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].b[4]
                                                            }
                                                            id="51"
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .b[4]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    51
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    51
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                          >
                                                            {card.card[0].b[4]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].i[4]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .i[4]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    52
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    52
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="52"
                                                          >
                                                            {card.card[0].i[4]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].n[3]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .n[3]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    53
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    53
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="53"
                                                          >
                                                            {card.card[0].n[3]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].g[4]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .g[4]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    54
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    54
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="54"
                                                          >
                                                            {card.card[0].g[4]}
                                                          </td>

                                                          <td
                                                            onClick={(id) =>
                                                              this.idValue(
                                                                card,
                                                                id
                                                              )
                                                            }
                                                            data-value={
                                                              card.card[0].o[4]
                                                            }
                                                            className={
                                                              this.state.gamenum.includes(
                                                                card.card[0]
                                                                  .o[4]
                                                              ) &&
                                                              this.state.arr.find(
                                                                (win) =>
                                                                  win.cardid ===
                                                                    card.cardid &&
                                                                  win.id.includes(
                                                                    55
                                                                  )
                                                              )
                                                                ? this.state
                                                                    .claimcard ===
                                                                    card.cardid &&
                                                                  this.state.patternmap.includes(
                                                                    55
                                                                  )
                                                                  ? "pat-active "
                                                                  : "digit user-clicked-num row-box_2 mr-2 mt-1 mb-1"
                                                                : "digit row-box_2 mr-2 mt-1 mb-1"
                                                            }
                                                            id="55"
                                                          >
                                                            {card.card[0].o[4]}
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <div className="game-btn">
                                                      {" "}
                                                      <button
                                                        disabled={
                                                          this.state
                                                            .bingobtndisable
                                                        }
                                                        className="btn  btn-lines mt-0 btn-block"
                                                        onClick={() =>
                                                          this.winbingo(
                                                            card,
                                                            getbingo.game
                                                              .pattern
                                                          )
                                                        }
                                                      >
                                                        Claim{" "}
                                                        {getbingo
                                                          ? getbingo.game
                                                              .pattern
                                                          : "Claim"}
                                                      </button>
                                                    </div>{" "}
                                                  </div>
                                                );
                                              }
                                            )
                                          : null}
                                      </div>
                                    </div>
                                  </div>
                                ) : (
                                  <div class="text-center">
                                    <Loader
                                      type="Bars"
                                      color="#FFFF"
                                      height={100}
                                      width={100}
                                    />
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div className="offset-md-3">
                        <img
                          src={require("../../assets/images/loader.gif")}
                          alt="loading"
                        />
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getbingo: state.getbingo,
    bingotick: state.bingotick,
  };
};

export default connect(mapStateToProps)(Bingo);
