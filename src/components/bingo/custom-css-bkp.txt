/* bingo CSS */
.select-card {
  position: relative;
  background: rgb(47, 10, 47);
  position: relative;
  height: 800px;
}

.card-bingo {
  padding: 3% 8%;
  margin: 1% 5%;
  background: linear-gradient(to bottom, #7d19bb, #3e143e);
  border-top: 8px #8e468e solid;
  border-radius: 0px 0px 10px 10px;
}
/* linear-gradient(to top, rgb(110, 0, 228), rgb(127, 14, 240); */

.card-bingo h1 {
  color: #f9d423;
  font-size: 56px;
  font-weight: 700;
  margin-bottom: 20px;
}
.title {
  color: white;
  font-weight: 800;
  border: 2px solid silver;
  text-align: center;
  border-radius: 10px 10px 0px 0px !important;
  background: #2f0a2f;
  margin-bottom: 3%;
}

.title h5 {
  font-weight: 700;
}

.btn-title {
  color: white;
  font-weight: 800;
  border: 2px solid silver;
  text-align: center;
  border-radius: 10px;
  background: #2f0a2f;
  margin-top: 3%;
}
.paytable {
  text-align: center;
  padding: 1% 5%;
  margin: 1% 5%;
  background: linear-gradient(to bottom, #450d56, #4b0e60);
}
.paytable p {
  font-size: 18px;
  color: #ffd300;
  margin: 5px;
  font-weight: 600;
}

.nums_card-1 {
  padding: 2% 6%;
}
.nums_block {
  border-radius: 0px 0px 10px 10px !important;
  background: #2f0a2f;
  margin-bottom: 3%;
  color: white;
  font-weight: 500;
  border: 2px solid silver;
  padding: 55px 12px;
  margin: 0px auto;
  justify-content: center;
}

.nums_block_1 {
  background: #e2bd0b;
  background: -webkit-linear-gradient(to top, #c76e00, #f6f923);
  background: linear-gradient(to top, #c76e00, #f6f923);
  text-align: center;
  padding: 8px 5px;
  border: 2px solid silver;
  border-radius: 10px 0px 0px 10px !important;
}
.nums_block_1 i {
  font-size: 22px;
  line-height: 26px;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.7);
}
.nums_block_2 {
  text-align: center;
  background: #e2bd0b;
  background: -webkit-linear-gradient(to top, #c76e00, #f6f923);
  background: linear-gradient(to top, #c76e00, #f6f923);
  font-size: 26px;
  font-weight: 700;
  border: 2px solid silver;
}

.quantity {
  background: #000;
  margin: 5px;
  border: 2px solid silver;
  padding: 0px;
  line-height: 30px;
}
.nums_block_3 {
  background: #e2bd0b;
  background: -webkit-linear-gradient(to top, #c76e00, #f6f923);
  background: linear-gradient(to top, #c76e00, #f6f923);
  text-align: center;
  padding: 8px 5px;
  border: 2px solid silver;
  border-radius: 0px 10px 10px 0px !important;
}
.nums_block_3 i {
  font-size: 22px;
  line-height: 26px;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.7);
}

.btn-play {
  background: #e2bd0b;
  background: -webkit-linear-gradient(to top, #c76e00, #f6f923);
  background: linear-gradient(to top, #c76e00, #f6f923);
  border: 2px solid silver;
  display: block;
  margin: 0px auto;
  padding: 2% 20%;
  font-size: 26px;
  text-transform: uppercase;
  font-weight: bold;
  color: #fff;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.7);
}

.btn-start {
  background: #e2bd0b;
  background: -webkit-linear-gradient(to top, #c76e00, #f6f923);
  background: linear-gradient(to top, #c76e00, #f6f923);
  border: 2px solid silver;
  padding: 1% 6%;
  font-size: 24px;
  text-transform: uppercase;
  font-weight: bold;
  color: #fff;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.7);
  z-index: 9999;
}
.digit_table {
  z-index: 1;
}
.game-btn button:hover {
  cursor: pointer !important;
  background: #333;
  color: #fff;
}

.row-box_2 {
  border: 1px solid #dbdce0;
  padding: 10px;
  height: 40px;
  text-align: center;
  font-size: 18px;
  font-weight: 800;
  color: #2b2b2b;
  background: #fff;
  width: 40px;
}
.row-box_2:nth-child(2n + 1) {
  background-color: #f3f3f3;
  border-bottom: 1px solid #c1c0c0;
}

.bingo_box {
  width: 100%;
}
.border-r {
  border-radius: 10px;
}

.bingo_box table {
  border-radius: 5px;
}

.bingo_box thead {
  font-size: 30px;
  background: #e2bd0b;
  background: -webkit-linear-gradient(to top, #c76e00, #f6f923);
  background: linear-gradient(to top, #c76e00, #f6f923);
  color: #fff;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.7);
  text-align: center;
}

.bingo_box thead th:first-child {
  background: linear-gradient(to top, #c76e00, #f6f923);
}
.bingo_box thead th:nth-child(2n + 1) {
  background: linear-gradient(to top, #c72e00, #f6f923);
}

.star {
  width: 30px;
}
