import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { toast } from "react-toastify";
import get_bingogame from "./../../redux/actions/getbingoActions";
import auth from "../../services/authService";
import bingo from "../../services/bingoservice";
import get_bingotick from "../../redux/actions/bingotickAction";
import Loader from "react-loader-spinner";

class Index extends Component {
  state = {
    selectedCards: null,
    card: 0,
    disable: false,
    games: [],
    gameid: "",
  };
  componentDidMount = async () => {
    try {
      const { getbingo } = await this.props;
      if (!getbingo) {
        await get_bingogame();
        await get_bingotick();
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        toast.error(ex.response.data, {
          position: toast.POSITION.BOTTOM_LEFT,
        });
        setTimeout(() => {
          window.location = "/index";
        }, 3000);
      }
    }
  };
  openBingo = async (g) => {
    this.setState({ gameid: g.game.gameid });
    toast.configure();
    if (!this.state.card) {
      toast.error("Please Select Tickets", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
    } else if (this.state.card >= 1) {
      const user = await auth.getCurrentUser();
      const obj = {
        gameid: g.game.gameid,
        totaltick: this.state.card.toString(),
        user: user.email,
      };

      try {
        var data = await bingo.sendgame(obj);

        if (data) {
          toast.success("Bought the tickets successfully", {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          setTimeout(() => {
            window.location = "/index";
          }, 3000);
        }
        this.setState({ disable: true });
      } catch (ex) {
        if (ex.response && ex.response.status === 400) {
          toast.error(ex.response.data, {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          setTimeout(() => {
            window.location = "/index";
          }, 3000);
        }
      }
    }
  };
  decrement = async (g) => {
    if (g.game.gameid !== this.state.gameid) {
      this.setState({ card: 0 });
      await this.setState({ gameid: g.game.gameid });
      //   var gid = this.props.getbingo.filter(
      //     (i) => i.game.gameid === g.game.gameid
      //   );
      if (this.state.card === 0) {
        this.setState({ card: 0 });
      }
      // if (this.state.card <= 8 && this.state.card > 0) {
      // 	var dec = this.state.card - 1;
      // 	await this.setState({ card: dec });
      // }
    } else {
      await this.setState({ gameid: g.game.gameid });

      //   const gid = this.props.getbingo.filter(
      //     (i) => i.game.gameid === g.game.gameid
      //   );

      if (this.state.card <= 8 && this.state.card > 0) {
        var inc = this.state.card - 1;
        await this.setState({ card: inc });
      }
    }
  };

  increment = async (g) => {
    var inc;
    toast.configure();
    if (g.game.gameid !== this.state.gameid) {
      this.setState({ card: 0 });
      await this.setState({ gameid: g.game.gameid });

      //   const gid = this.props.getbingo.filter(
      //     (i) => i.game.gameid === g.game.gameid
      //   );

      if (this.state.card <= 8) {
        inc = this.state.card + 1;
        await this.setState({ card: inc });
      }
    } else {
      await this.setState({ gameid: g.game.gameid });

      //   const gid = this.props.getbingo.filter(
      //     (i) => i.game.gameid === g.game.gameid
      //   );

      if (this.state.card <= 8) {
        inc = this.state.card + 1;
        await this.setState({ card: inc });
      }
    }
  };
  resumeBingo = async (g) => {
    toast.configure();
    const btick = await this.props.bingotick;
    setTimeout(async () => {
      if (btick && btick.length >= 1) {
        await this.props.history.push({
          pathname: "/bingo",
          state: { game: g },
        });
      } else {
        toast.error("Please Wait For Next Game", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      }
    }, 300);
  };
  render() {
    const { getbingo } = this.props;

    return (
      <React.Fragment>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="tambola-start-bottom mt-0">
                      <h2 className=" text-center">Game Schedule</h2>
                    </div>
                    <div className="board-border1">
                      <div className=" buy_tickets-table">
                        {getbingo ? (
                          <table className="table text-white text-center">
                            <thead>
                              <tr>
                                <th>DATE & TIME </th>
                                <th>GAME ID</th>
                                <th>PATTERN</th>
                                <th>Tickets Bought</th>
                                <th>Num Of Tickets</th>
                                <th>GAME STATUS</th>
                              </tr>
                            </thead>
                            {getbingo
                              ? getbingo.map((g) => {
                                  return (
                                    <tbody>
                                      <tr>
                                        <td className="pt-3">
                                          {" "}
                                          <br></br>
                                          {moment(g.game.date).format(
                                            "D MMM YYYY hh:mm A"
                                          )}
                                        </td>
                                        <td className="pt-3">
                                          {" "}
                                          <br></br>{" "}
                                          {getbingo ? (
                                            g.game.gameid
                                          ) : (
                                            <div>Please Wait..</div>
                                          )}
                                        </td>
                                        <td>
                                          {getbingo ? (
                                            <img
                                              src={require(`../../assets/images/${g.game.pattern}.png`)}
                                              alt="img"
                                            />
                                          ) : (
                                            <div className="pt-4">
                                              Please Wait..
                                            </div>
                                          )}
                                        </td>

                                        <td className="pt-4">
                                          {" "}
                                          <div className="nums_circle">
                                            {" "}
                                            {getbingo
                                              ? g.user
                                              : this.state.card}{" "}
                                          </div>{" "}
                                        </td>

                                        <td className="pt-2">
                                          <div className="col-lg-12 px-4">
                                            <div className="input-group">
                                              {this.state.card <= 0 ? (
                                                <span className="input-group-btn">
                                                  <button
                                                    type="button"
                                                    className="quantity-left-minus btn btn-danger btn-number not-allowed"
                                                    // onClick={this.decrement}
                                                    disabled={true}
                                                  >
                                                    <span className="fa fa-minus"></span>
                                                  </button>
                                                </span>
                                              ) : (
                                                <span className="input-group-btn">
                                                  <button
                                                    type="button"
                                                    className="quantity-left-minus btn btn-danger btn-number "
                                                    onClick={() =>
                                                      this.decrement(g)
                                                    }
                                                  >
                                                    <span className="fa fa-minus"></span>
                                                  </button>
                                                </span>
                                              )}
                                              <input
                                                className="form-control input-number"
                                                readOnly
                                                value={
                                                  g.game.gameid !==
                                                  this.state.gameid
                                                    ? 0
                                                    : this.state.card
                                                }
                                              />
                                              {this.state.card === 8 ? (
                                                <span className="input-group-btn">
                                                  <button
                                                    type="button"
                                                    className="quantity-right-plus btn btn-success btn-number not-allowed"
                                                    disabled={true}
                                                  >
                                                    <span className="fa fa-plus"></span>
                                                  </button>
                                                </span>
                                              ) : (
                                                <span className="input-group-btn">
                                                  <button
                                                    type="button"
                                                    className="quantity-right-plus btn btn-success btn-number"
                                                    onClick={() =>
                                                      this.increment(g)
                                                    }
                                                  >
                                                    <span className="fa fa-plus"></span>
                                                  </button>
                                                </span>
                                              )}
                                            </div>

                                            <button
                                              className="btn btn-play d-block w-100 mt-2"
                                              onClick={() => this.openBingo(g)}
                                              disabled={this.state.disable}
                                            >
                                              {" "}
                                              Buy Ticket
                                            </button>
                                          </div>
                                          {/* <div className="pt-2">Please Wait..</div> */}
                                        </td>
                                        <td>
                                          {getbingo ? (
                                            g.game ? (
                                              g.game.status === "pending" ? (
                                                <button
                                                  className="btn btn-green bg-orange"
                                                  // onClick={this.openBingo}
                                                  disabled={this.state.disable}
                                                >
                                                  Pending
                                                </button>
                                              ) : getbingo ? (
                                                <button
                                                  style={{
                                                    background: "#28a745",
                                                  }}
                                                  className="btn btn-green bg-orange"
                                                  onClick={() =>
                                                    this.resumeBingo(g)
                                                  }
                                                  disabled={this.state.disable}
                                                >
                                                  {" "}
                                                  Resume
                                                </button>
                                              ) : (
                                                <div className="pt-4">
                                                  Please Wait..
                                                </div>
                                              )
                                            ) : (
                                              <div className="pt-4">
                                                Please Wait..
                                              </div>
                                            )
                                          ) : (
                                            <div className="pt-4">
                                              Please Wait..
                                            </div>
                                          )}
                                        </td>
                                      </tr>
                                    </tbody>
                                  );
                                })
                              : null}
                          </table>
                        ) : (
                          <div className="offset-md-5">
                            <Loader
                              type="Bars"
                              color="#FFFF"
                              height={100}
                              width={100}
                            />
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getbingo: state.getbingo,
    bingotick: state.bingotick,
  };
};

export default connect(mapStateToProps)(Index);
