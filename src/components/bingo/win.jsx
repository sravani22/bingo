import React, { Component } from "react";
import Moment from "react-moment";
import { connect } from "react-redux";

import clock from "../../assets/images/clock1.png";

import get_bingogame from "./../../redux/actions/getbingoActions";

class Win extends Component {
  state = { winner: "" };
  componentDidMount = async () => {
    console.log(this.props.location.state.name);
    const { getbingo } = await this.props;
    // var winname = data.winners.map(async (win) => {
    //   if (win.winpat === "Bingo") {
    //     var nam = ` ${win.name} ! `;
    await this.setState({ winner: this.props.location.state.name });
    //   return win;
    // }
    // // setTimeout(() => this.props.history.push("/index"), 3000);
    // });
    if (!getbingo) {
      await get_bingogame();
    }
  };

  render() {
    const { getbingo } = this.props;

    return (
      <React.Fragment>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="title tambola-start">
                      <div className="title-row clearfix">
                        <div className="gamenum">
                          <div className="tgame-no">
                            Game no.{getbingo.gameid}
                          </div>
                        </div>
                        <div className="date-time">
                          <div className="game-details">
                            <div className="clearfix">
                              <div className="take-left">
                                <i className="fa fa-calendar-alt" />
                                <span className="ml-2 current-date">
                                  <Moment format="D MMM YYYY" withTitle />
                                </span>
                              </div>
                              <div className="take-right">
                                <span className="icon-t">
                                  <img src={clock} alt="time" />
                                </span>{" "}
                                <span className=" current-time">
                                  <Moment format="hh:mm A" withTitle />{" "}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tambola-start-bottom mt-0 pb-5 pt-5">
                      <canvas id="confetti" width="1" height="1"></canvas>
                      <div className="row pb-5">
                        <div className="tambola-info col-md-10 pt-5 pb-5">
                          <h1>Bingo Won By</h1>
                          <div className="game-startat">
                            <p>
                              <span
                                class="win_text"
                                style={{ fontSize: "56px" }}
                              >
                                {this.state.winner}
                              </span>
                              &nbsp;
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getbingo: state.getbingo,
  };
};

export default connect(mapStateToProps)(Win);
