import React, { Component } from "react";
import { toast } from "react-toastify";
import { normalizeUnits } from "moment";
const WinningPatterns = {
  HorizontalPattern(arr) {
    toast.configure();
    const b = [11, 12, 13, 14, 15];
    const i = [21, 22, 23, 24, 25];
    const n = [31, 32, 34, 35];
    const g = [41, 42, 43, 44, 45];
    const o = [51, 52, 53, 54, 55];
    const arry = [b, i, n, g, o];
    const result = arry.filter(function (el) {
      var r = el.every((elem) => arr.includes(elem));
      return r;
    });
    console.log(result);
    // var b = hzpattern.every((elem) => arr.indexOf(elem) > -1);
    // var i = hzpattern2.every((elem) => arr.indexOf(elem) > -1);
    // var n = hzpattern3.every((elem) => arr.indexOf(elem) > -1);
    // var g = hzpattern4.every((elem) => arr.indexOf(elem) > -1);
    // var o = hzpattern5.every((elem) => arr.indexOf(elem) > -1);

    // const check = b || i || n || g || o;
    // const anytwo =
    //   (b && i) ||
    //   (b && n) ||
    //   (b && g) ||
    //   (b && o) ||
    //   (i && n) ||
    //   (i && g) ||
    //   (i && o) ||
    //   (n && g) ||
    //   (n && o) ||
    //   (g && o);

    // const anythree =
    //   (b && i && n) ||
    //   (b && i && g) ||
    //   (b && i && o) ||
    //   (i && n && g) ||
    //   (i && n && o) ||
    //   (n && g && o);

    // const anyfour =
    //   (b && i && n && g) ||
    //   (b && i && n && o) ||
    //   (b && i && g && o) ||
    //   (b && n && g && o) ||
    //   (i && n && g && o);

    // if (anyfour) {
    //   return "h4-pattren";
    // } else if (anythree) {
    //   return "h3-pattren";
    // } else if (anytwo) {
    //   return "h2-pattren";
    // } else if (check) {
    //   return "h-pattren";
    // } else {
    //   toast.error("please check  u r pattern", {
    //     position: toast.POSITION.BOTTOM_LEFT,
    //   });
    // }
  },

  VerticalPatterns(arr) {
    toast.configure();
    const vzpattern = [11, 21, 31, 41, 51];
    const vzpattern2 = [12, 22, 32, 42, 52];
    const vzpattern3 = [13, 23, 43, 53];
    const vzpattern4 = [14, 24, 34, 44, 54];
    const vzpattern5 = [15, 25, 35, 45, 55];

    const check =
      vzpattern.every((elem) => arr.includes(elem)) ||
      vzpattern2.every((elem) => arr.includes(elem)) ||
      vzpattern3.every((elem) => arr.includes(elem)) ||
      vzpattern4.every((elem) => arr.includes(elem)) ||
      vzpattern5.every((elem) => arr.includes(elem));

    if (check) {
      return "v-pattren";
    } else {
      toast.error("please check  u r pattern", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
    }
  },
  DiagonalPattern(arr) {
    toast.configure();
    const dzpattern = [11, 22, 44, 55];
    const dzpattern2 = [51, 42, 24, 15];
    const dzpattern3 = [11, 22, 44, 55, 51, 42, 24, 15];

    var check1 = dzpattern.every((elem) => arr.includes(elem));
    var check2 = dzpattern2.every((elem) => arr.includes(elem));

    console.log(check1);
    if (check1 && !check2) {
      return dzpattern;
    } else if (!check1 && check2) {
      return dzpattern2;
    } else if (check1 && check2) {
      return dzpattern3;
    } else {
      toast.error("please check  u r pattern", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  FourCorners(arr) {
    toast.configure();
    const dzpattern = [11, 15, 51, 55];
    var check1 = dzpattern.every((elem) => arr.includes(elem));

    if (check1) {
      return dzpattern;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  InsideBox(arr) {
    toast.configure();
    const dzpattern = [22, 23, 24, 32, 34, 42, 43, 44];
    var check1 = dzpattern.every((elem) => arr.includes(elem));

    if (check1) {
      return dzpattern;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  DiamondPattern(arr) {
    toast.configure();
    const dzpattern = [13, 22, 31, 42, 53, 44, 35, 24];
    var check1 = dzpattern.every((elem) => arr.includes(elem));

    if (check1) {
      return dzpattern;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  SmallCrossInside(arr) {
    toast.configure();
    const dzpattern = [23, 32, 43, 34];
    var check1 = dzpattern.every((elem) => arr.includes(elem));

    if (check1) {
      return dzpattern;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  Crucifix(arr) {
    toast.configure();
    const dzpattern = [13, 23, 43, 53, 31, 32, 34, 35];
    var check1 = dzpattern.every((elem) => arr.includes(elem));

    if (check1) {
      return dzpattern;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  BoxofFour(arr) {
    toast.configure();
    const vzpattern = [11, 12, 22, 22];
    const vzpattern2 = [14, 15, 24, 25];
    const vzpattern3 = [41, 42, 51, 52];
    const vzpattern4 = [44, 45, 54, 55];

    var check1 = vzpattern.every((elem) => arr.includes(elem));
    var check2 = vzpattern2.every((elem) => arr.includes(elem));
    var check3 = vzpattern3.every((elem) => arr.includes(elem));
    var check4 = vzpattern4.every((elem) => arr.includes(elem));

    if (check1) {
      return vzpattern;
    } else if (check2 && !check1) {
      return vzpattern2;
    } else if (check3 && !check1 && !check2) {
      return vzpattern3;
    } else if (check4 & !check1 && !check2 && !check3) {
      return vzpattern3;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  BoxofNine(arr) {
    toast.configure();
    const upperLeft = [11, 12, 13, 21, 22, 23, 31, 32];
    const upperRight = [13, 14, 15, 23, 24, 25, 34, 35];
    const LowerLeft = [31, 32, 41, 42, 43, 51, 52, 53];
    const LowerRight = [34, 35, 43, 44, 53, 45, 54, 55];

    var check1 = upperLeft.every((elem) => arr.includes(elem));
    var check2 = upperRight.every((elem) => arr.includes(elem));
    var check3 = LowerLeft.every((elem) => arr.includes(elem));
    var check4 = LowerRight.every((elem) => arr.includes(elem));

    if (check1) {
      return upperLeft;
    } else if (check2 && !check1) {
      return upperRight;
    } else if (check3 && !check1 && !check2) {
      return LowerLeft;
    } else if (check4 & !check1 && !check2 && !check3) {
      return LowerRight;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  ShadedPatt(arr) {
    toast.configure();
    const upperLeft = [11, 12, 13, 14, 15, 21, 22, 23, 24, 31, 32, 41, 42, 51];
    const upperRight = [11, 12, 13, 14, 15, 22, 23, 24, 25, 34, 35, 44, 45, 55];
    const LowerLeft = [11, 21, 22, 31, 32, 41, 42, 43, 44, 51, 52, 53, 54, 55];
    const LowerRight = [15, 24, 25, 34, 35, 42, 43, 44, 45, 51, 52, 53, 54, 55];

    var check1 = upperLeft.every((elem) => arr.includes(elem));
    var check2 = upperRight.every((elem) => arr.includes(elem));
    var check3 = LowerLeft.every((elem) => arr.includes(elem));
    var check4 = LowerRight.every((elem) => arr.includes(elem));

    if (check1) {
      return upperLeft;
    } else if (check2 && !check1) {
      return upperRight;
    } else if (check3 && !check1 && !check2) {
      return LowerLeft;
    } else if (check4 & !check1 && !check2 && !check3) {
      return LowerRight;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  AnytwoRowsPatt(arr) {
    toast.configure();
    const b = [11, 12, 13, 14, 15];
    const i = [21, 22, 23, 24, 25];
    const n = [31, 32, 34, 35];
    const g = [41, 42, 43, 44, 45];
    const o = [51, 52, 53, 54, 55];
    const arry = [b, i, n, g, o];

    var check1 = b.every((elem) => arr.includes(elem));
    var check2 = i.every((elem) => arr.includes(elem));
    var check3 = n.every((elem) => arr.includes(elem));
    var check4 = g.every((elem) => arr.includes(elem));
    var check5 = o.every((elem) => arr.includes(elem));

    if (check1 && check5) {
      return b, o;
    } else if (check2 && check4) {
      return i, o;
    } else if (check1 && check3 && check5) {
      return b, n, o;
    } else {
      toast.error("Invalid Pattern ", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      return null;
    }
  },
  AnytwoColPatterns(arr) {
    toast.configure();
    const b = [11, 21, 31, 41, 51];
    const i = [12, 22, 32, 42, 52];
    const n = [13, 23, 43, 53];
    const g = [14, 24, 34, 44, 54];
    const o = [15, 25, 35, 45, 55];

    const check1 = b.every((elem) => arr.includes(elem));
    const check2 = i.every((elem) => arr.includes(elem));
    const check3 = n.every((elem) => arr.includes(elem));
    const check4 = g.every((elem) => arr.includes(elem));
    const check5 = o.every((elem) => arr.includes(elem));

    if (check) {
      return "v-pattren";
    } else {
      toast.error("please check  u r pattern", {
        position: toast.POSITION.BOTTOM_LEFT,
      });
    }
  },
};

export { WinningPatterns };
