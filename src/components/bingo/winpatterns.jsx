import React, { Component } from "react";
import { toast } from "react-toastify";
const WinPatterns = {
	LeftDiagonal(arr) {
		toast.configure();
		const dzpattern = [11, 22, 44, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	RightDiagonal(arr) {
		toast.configure();
		const dzpattern = [15, 24, 42, 51];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Corners4(arr) {
		toast.configure();
		const dzpattern = [11, 15, 51, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	InsideBox(arr) {
		toast.configure();
		const dzpattern = [22, 23, 24, 32, 34, 42, 43, 44];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Cross(arr) {
		toast.configure();
		const dzpattern = [11, 22, 44, 55, 51, 42, 24, 15];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Diamond(arr) {
		toast.configure();
		const dzpattern = [13, 22, 31, 42, 53, 44, 35, 24];
		var check1 = dzpattern.every((elem) => arr.includes(elem));

		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	SmallCrossInside(arr) {
		toast.configure();
		const dzpattern = [23, 32, 43, 34];
		var check1 = dzpattern.every((elem) => arr.includes(elem));

		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	BColumn(arr) {
		toast.configure();
		const dzpattern = [11, 21, 31, 41, 51];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	IColumn(arr) {
		toast.configure();
		const dzpattern = [12, 22, 32, 42, 52];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	NColumn(arr) {
		toast.configure();
		const dzpattern = [13, 23, 43, 53];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	GColumn(arr) {
		toast.configure();
		const dzpattern = [14, 24, 34, 44, 54];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	OColumn(arr) {
		toast.configure();
		const dzpattern = [15, 25, 35, 45, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	FirstRow(arr) {
		toast.configure();
		var hzpattern = [11, 12, 13, 14, 15];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	SecondRow(arr) {
		toast.configure();
		var hzpattern = [21, 22, 23, 24, 25];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	ThirdRow(arr) {
		toast.configure();
		var hzpattern = [31, 32, 34, 35];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	FourthRow(arr) {
		toast.configure();
		var hzpattern = [41, 42, 43, 44, 45];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	FifthRow(arr) {
		toast.configure();
		var hzpattern = [51, 52, 53, 54, 55];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	Crucifix(arr) {
		toast.configure();
		const dzpattern = [13, 23, 43, 53, 31, 32, 34, 35];
		var check1 = dzpattern.every((elem) => arr.includes(elem));

		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof4UpperLeft(arr) {
		toast.configure();
		const dzpattern = [11, 12, 21, 22];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof4UpperRight(arr) {
		toast.configure();
		const dzpattern = [14, 15, 24, 25];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof4LowerLeft(arr) {
		toast.configure();
		const dzpattern = [41, 42, 51, 52];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof4LowerRight(arr) {
		toast.configure();
		const dzpattern = [44, 45, 54, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof9UpperLeft(arr) {
		toast.configure();
		const dzpattern = [11, 12, 13, 21, 22, 23, 31, 32];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof9UpperRight(arr) {
		toast.configure();
		const dzpattern = [13, 14, 15, 23, 24, 25, 34, 35];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof9LowerLeft(arr) {
		toast.configure();
		const dzpattern = [31, 32, 41, 42, 43, 51, 52, 53];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	Boxof9LowerRight(arr) {
		toast.configure();
		const dzpattern = [34, 35, 43, 44, 53, 45, 54, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	ShadedUpperLeft(arr) {
		toast.configure();
		const dzpattern = [11, 12, 13, 14, 15, 21, 22, 23, 24, 31, 32, 41, 42, 51];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	ShadedLowerLeft(arr) {
		toast.configure();
		const dzpattern = [11, 21, 22, 31, 32, 41, 42, 43, 44, 51, 52, 53, 54, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	ShadedUpperRight(arr) {
		toast.configure();
		const dzpattern = [11, 12, 13, 14, 15, 22, 23, 24, 25, 34, 35, 44, 45, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},
	ShadedLowerRight(arr) {
		toast.configure();
		const dzpattern = [15, 24, 25, 34, 35, 42, 43, 44, 45, 51, 52, 53, 54, 55];
		var check1 = dzpattern.every((elem) => arr.includes(elem));
		if (check1) {
			return dzpattern;
		} else {
			toast.error("Invalid Pattern ", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
			return null;
		}
	},

	TwoRowsTopBottom(arr) {
		toast.configure();
		var hzpattern = [11, 12, 13, 14, 15, 51, 52, 53, 54, 55];

		var check = hzpattern.every((elem) => arr.includes(elem));

		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	TwoRowsMid(arr) {
		toast.configure();
		var hzpattern = [21, 22, 23, 24, 25, 41, 42, 43, 44, 45];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	ThreeRowsShaded(arr) {
		toast.configure();
		const hzpattern = [11, 12, 13, 14, 15, 31, 32, 34, 35, 51, 52, 53, 54, 55];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	TwoColumnsShadedBAndO(arr) {
		toast.configure();
		const hzpattern = [11, 21, 31, 41, 51, 15, 25, 35, 45, 55];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
	TwoColumnsShadedIAndG(arr) {
		toast.configure();
		const hzpattern = [12, 22, 32, 42, 52, 14, 24, 34, 44, 54];

		var check = hzpattern.every((elem) => arr.includes(elem));

		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},

	BlackOut(arr) {
		toast.configure();
		const hzpattern = [
			11,
			12,
			13,
			14,
			15,
			21,
			22,
			23,
			24,
			25,
			31,
			32,
			34,
			35,
			41,
			42,
			43,
			44,
			45,
			51,
			52,
			53,
			54,
			55,
		];
		var check = hzpattern.every((elem) => arr.includes(elem));
		if (check) {
			return hzpattern;
		} else {
			toast.error("Invalid Pattern", {
				position: toast.POSITION.BOTTOM_LEFT,
			});
		}
	},
};

export { WinPatterns };
