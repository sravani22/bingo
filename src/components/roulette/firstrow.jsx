import React, { Component } from "react";

class Firstrow extends Component {
  state = {
    selected: [],

    num: true,
  };
  numbers = () => {
    var rows = [];
    for (var i = 3; i <= 36; i++) {
      if (i % 3 === 0) {
        rows.push(
          <span
            onClick={this.numbvalue}
            id={i}
            className={
              i === 6 || i === 15 || i === 24 || i === 33
                ? "col text-dark"
                : "col text-danger"
            }
          >
            {this.state.selected.includes(i) === true ? (
              <div class="chip-2"></div>
            ) : (
              <div class="dolly"></div>
            )}
            {i}
          </span>
        );
      }
    }
    return rows;
  };
  numbers2 = () => {
    var rows = [];
    for (var i = 2; i <= 37; i++) {
      if (i % 3 === 0) {
        rows.push(
          <span
            onClick={this.numbvalue}
            id={i - 1}
            className={
              i - 1 === 2 ||
              i - 1 === 8 ||
              i - 1 === 11 ||
              i - 1 === 17 ||
              i - 1 === 20 ||
              i - 1 === 26 ||
              i - 1 === 29 ||
              i - 1 === 35
                ? "col text-dark"
                : "col text-danger"
            }
          >
            {this.state.selected.includes(i - 1) === true ? (
              <div class="chip-2"></div>
            ) : (
              <div class="dolly"></div>
            )}
            {i - 1}
          </span>
        );
      }
    }
    return rows;
  };
  numbers3 = () => {
    var rows = [];
    for (var i = 2; i <= 38; i++) {
      if (i % 3 === 0) {
        rows.push(
          <span
            onClick={this.numbvalue}
            id={i - 2}
            className={
              i - 2 === 4 ||
              i - 2 === 10 ||
              i - 2 === 13 ||
              i - 2 === 22 ||
              i - 2 === 28 ||
              i - 2 === 31
                ? "col text-dark"
                : "col text-danger"
            }
          >
            {this.state.selected.includes(i - 2) === true ? (
              <div class="chip-2"></div>
            ) : (
              <div class="dolly"></div>
            )}
            {i - 2}
          </span>
        );
      }
    }
    return rows;
  };
  numbvalue = async (e) => {
    console.log(e.target.id);
    this.setState({ num: !this.state.num });

    if (!this.state.selected.includes(parseFloat(e.target.id))) {
      await this.setState({
        selected: [...this.state.selected, parseFloat(e.target.id)],
      });
      this.props.history.push({
        pathname: "/roulette",
        state: { data: this.state.selected },
      });
    }
  };
  render() {
    return (
      <React.Fragment>
        <div className="numbers col-md-12 d-flex">{this.numbers()}</div>
        <div className="numbers col-md-12 d-flex">{this.numbers2()}</div>
        <div className="numbers col-md-12 d-flex">{this.numbers3()}</div>
      </React.Fragment>
    );
  }
}

export default Firstrow;
