import React, { Component } from "react";

class BetTable extends Component {
  state = {
    chip: false,
    selected: [],
  };
  // first = () => {
  //   var rows = [];
  //   for (var i = 1; i <= 4; i++) {
  //     rows.push(
  //       <div className="form-check form-check-inline ">
  //         <label className="form-check-label">
  //           <span className="number-name">{i}</span>
  //         </label>
  //       </div>
  //     );
  //   }
  //   return rows;
  // };
  numbvalue = async (e) => {
    // this.setState({ chip: true });
    if (!this.state.selected.includes(e)) {
      await this.setState({ selected: [...this.state.selected, e] });
    }
    console.log(this.state.selected);
  };
  render() {
    return (
      <React.Fragment>
        <div className="container text-light">
          <div className="row the-table">
            <div className="col">Wheel</div>
            <div className="col">
              <div className="row other-numbers">
                <div
                  className="form-check form-check-inline col-sm-2"
                  onClick={() => this.numbvalue("1 to 18")}
                >
                  <label class="form-check-label ">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value="1 to 18"
                    />
                    {this.state.selected.includes("1 to 18") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span class="number-name">1 to 18</span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-sm-2"
                  onClick={() => this.numbvalue("Even")}
                >
                  <label class="form-check-label ">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      value="Even"
                    />
                    {this.state.selected.includes("Even") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span class="number-name">Even</span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-sm-2"
                  onClick={() => this.numbvalue("Red")}
                >
                  <label class="form-check-label ">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      value="Red"
                    />
                    {this.state.selected.includes("Red") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span class="number-name" for="reds">
                      Red
                    </span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-sm-2"
                  onClick={() => this.numbvalue("Black")}
                >
                  <label
                    class="form-check-label "
                    title="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18"
                  >
                    <input
                      class="form-check-input"
                      type="checkbox"
                      value="Black"
                    />
                    {this.state.selected.includes("Black") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span class="number-name" for="blacks">
                      Black
                    </span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-sm-2"
                  onClick={() => this.numbvalue("Odd")}
                >
                  <label
                    class="form-check-label "
                    title="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18"
                  >
                    <input
                      class="form-check-input"
                      type="checkbox"
                      value="Odd"
                    />
                    {this.state.selected.includes("Odd") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span class="number-name">Odd</span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-sm-2"
                  onClick={() => this.numbvalue("19 to 36")}
                >
                  <label class="form-check-label ">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      value="19 to 36"
                    />
                    {this.state.selected.includes("19 to 36") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span class="number-name">19 to 36</span>
                  </label>
                </div>
              </div>
              <div className="row other-numbers">
                <div
                  className="form-check form-check-inline col-md-4 "
                  onClick={() => this.numbvalue("1st Dozen")}
                >
                  <label className="form-check-label ">
                    <input
                      type="checkbox"
                      value="1st Dozen"
                      class="form-check-input"
                    />
                    {this.state.selected.includes("1st Dozen") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span className="number-name">1st Dozen</span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-md-4"
                  onClick={() => this.numbvalue("2nd Dozen")}
                >
                  <label className="form-check-label ">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value="2nd Dozen"
                    />
                    {this.state.selected.includes("2nd Dozen") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span className="number-name">2nd Dozen</span>
                  </label>
                </div>
                <div
                  className="form-check form-check-inline col-md-4"
                  onClick={() => this.numbvalue("3rd Dozen")}
                >
                  <label className="form-check-label ">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value="3rd Dozen"
                    />
                    {this.state.selected.includes("3rd Dozen") === true ? (
                      <span class="chip"></span>
                    ) : (
                      <span class="dolly"></span>
                    )}
                    <span className="number-name">3rd Dozen</span>
                  </label>
                </div>
              </div>
              <div className="all-numbers">
                <div>
                  <div class="number-zero columns">
                    <div class="form-check form-check-inline ">
                      <label
                        class="form-check-label number-0"
                        for="0"
                        title="0"
                      >
                        <input
                          class="form-check-input"
                          type="checkbox"
                          id="0"
                          value="0"
                        />
                        <span
                          class="number-name mt-1"
                          style={{ color: "green" }}
                        >
                          0
                        </span>
                        <span class="dolly"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="third-column columns">
                  <div
                    className={
                      this.state.selected.includes("3") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("3")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="3"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("3") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("3") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"3"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("6") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("6")}
                  >
                    <label className="form-check-label" value={6}>
                      {" "}
                      <input
                        type="checkbox"
                        value="6"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("6") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("6") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"6"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("9") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("9")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="9"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("9") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("9") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"9"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("12") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("12")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="12"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("12") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("12") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"12"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("15") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("15")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="15"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("15") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("15") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"15"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("18") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("18")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="18"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("18") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("18") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"18"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("21") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("21")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="21"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("21") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("21") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"21"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("24") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("24")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="24"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("24") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("24") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"24"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("27") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("27")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="27"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("27") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("27") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"27"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("30") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("30")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="30"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("30") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("30") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"30"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("33") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("33")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="33"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("33") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("33") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"33"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("36") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("36")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="36"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("36") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("36") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"36"}
                      </span>
                    </label>
                  </div>
                  <div
                    className="form-check form-check-inline "
                    onClick={() => this.numbvalue("3rd")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="3rd"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("3rd") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span className="number-name1" htmlFor={"1to18"}>
                        {"3rd"}
                      </span>
                    </label>
                  </div>
                </div>

                <div className="third-column columns ">
                  <div
                    className={
                      this.state.selected.includes("2") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("2")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="2"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("2") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("2") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"2"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("5") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("5")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="5"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("5") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("5") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"5"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("8") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("8")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="8"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("8") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("8") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"8"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("11") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("11")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="11"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("11") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("11") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"11"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("14") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("14")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="14"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("14") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("14") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"14"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("17") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("17")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="17"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("17") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("17") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"17"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("20") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("20")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="20"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("20") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("20") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"20"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("23") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("23")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="23"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("23") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("23") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"23"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("26") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("26")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="26"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("26") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("26") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"26"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("29") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("29")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="29"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("29") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("29") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"29"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("32") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("32")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="32"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("32") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("32") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"32"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("35") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("35")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="35"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("35") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("35") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"35"}
                      </span>
                    </label>
                  </div>
                  <div
                    className="form-check form-check-inline "
                    onClick={() => this.numbvalue("2nd")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="2nd"
                        className="form-check-input"
                      />
                      {this.state.chip === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span className="number-name1" htmlFor={"1to18"}>
                        {"2nd"}
                      </span>
                    </label>
                  </div>
                </div>

                <div className="third-column columns">
                  <div
                    className={
                      this.state.selected.includes("1") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("1")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="1"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("1") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("1") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"1"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("4") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("4")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="4"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("4") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("4") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"4"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("7") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("7")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="7"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("7") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("7") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"7"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("10") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("10")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="10"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("10") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("10") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"10"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("13") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("13")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="13"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("13") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("13") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"13"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("16") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("16")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="16"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("16") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("16") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"16"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("19") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("19")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="19"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("19") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("19") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"19"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("22") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("22")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="22"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("22") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("22") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"22"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("25") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("25")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="25"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("25") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("25") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"25"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("28") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("28")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="28"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("28") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("28") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"28"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("31") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("31")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="31"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("31") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("31") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"blacks"}
                      >
                        {"31"}
                      </span>
                    </label>
                  </div>
                  <div
                    className={
                      this.state.selected.includes("34") === true
                        ? "form-check form-check-inline w-50"
                        : "form-check form-check-inline"
                    }
                    onClick={() => this.numbvalue("34")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="34"
                        className="form-check-input"
                      />
                      {this.state.selected.includes("34") === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span
                        className={
                          this.state.selected.includes("34") === true
                            ? "d-none "
                            : "number-name "
                        }
                        htmlFor={"reds"}
                      >
                        {"34"}
                      </span>
                    </label>
                  </div>
                  <div
                    className="form-check form-check-inline "
                    onClick={() => this.numbvalue("1st")}
                  >
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        value="1st"
                        className="form-check-input"
                      />
                      {this.state.chip === true ? (
                        <span class="chip"></span>
                      ) : (
                        <span class="dolly"></span>
                      )}
                      <span className="number-name1" htmlFor={"1to18"}>
                        {"1st"}
                      </span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BetTable;
