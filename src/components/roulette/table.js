import React, { Component } from "react";

import "../../assets/roulette/css/style.css";
// import Wheel from "./wheel";
import sound from "../../assets/roulette/sounds/chip-effect.mp3";
import wheelsound from "../../assets/roulette/sounds/ball-effect.mp3";
import Sound from "react-sound";
import { Wheel } from "react-custom-roulette";

var myArray = [
  32,
  15,
  19,
  4,
  21,
  2,
  25,
  17,
  34,
  6,
  27,
  13,
  36,
  11,
  30,
  8,
  23,
  10,
  5,
  24,
  16,
  33,
  1,
  20,
  14,
  31,
  9,
  22,
  18,
  29,
  7,
  28,
  12,
  35,
  3,
  26,
];
var newarr = [
  {
    option: "0",
    style: { backgroundColor: "green", textColor: "black" },
  },
];
var arr = myArray.forEach((element) => {
  newarr.push({ option: element });
});

class BetTable extends Component {
  state = {
    selected: [],
    playStatus: Sound.status.PLAYING,
    position: 0,
    volume: 50,
    loop: true,
    playbackRate: 1,
    color: [],
    wheel: false,
    stopatnum: 10,
    black: [],
  };

  numbvalue = async (e, id) => {
    if (!this.state.selected.includes(e)) {
      await this.setState({
        selected: [...this.state.selected, e],
      });
    }
  };
  get_random = function (list) {
    return list[Math.floor(Math.random() * list.length)];
  };
  clearbets = () => {
    this.setState({ selected: [] });
  };
  spinwheel = () => {
    this.setState({ wheel: true });
    this.playwheel();

    // setTimeout(() => {
    //   this.setState({ wheel: false });
    // }, 2000);
  };
  playwheel = () => {
    return (
      <Sound
        url={wheelsound}
        playStatus={this.state.playStatus}
        position={this.state.position}
        volume={this.state.volume}
        playbackRate={this.state.playbackRate}
        loop={this.state.loop}
        onPlaying={({ position }) => this.setState({ position })}
        autoLoad={true}
      />
    );
  };
  render() {
    let names = [
      "1to18",
      "Even",
      "Red",
      "Black",
      "Odd",
      "19to36",
      "1st Dozen",
      "2nd Dozen",
      "3rd Dozen",
      "1st",
      "2nd",
      "3rd",
    ];
    return (
      <React.Fragment>
        <div>
          <div>
            <div className="head">
              <h1 className=" text-center"> Roulette Presentation</h1>
              <h2 className=" text-center"> Try your luck!</h2>
            </div>
            <div className="bought">
              <h4 className="text-dark">
                {" "}
                You have : &nbsp; <span className="h2 text-danger"> 20 </span>
              </h4>
            </div>

            <div className="col-md-9 m-auto mobile-sm">
              <div className="game-card">
                <div className="row">
                  <div className="col-md-4 ">
                    <div className="wheel">
                      <div className="spinner-wheel">
                        <Wheel
                          mustStartSpinning={
                            this.state.wheel === true ? true : false
                          }
                          prizeNumber={
                            myArray.indexOf(this.state.stopatnum) + 1
                          }
                          data={newarr}
                          backgroundColors={["#3e3e3e", "#df3428"]}
                          textColors={["#ffffff"]}
                          textDistance={80}
                          innerRadius={35}
                          outerBorderWidth={10}
                          innerBorderWidth={30}
                          perpendicularText={true}
                          fontSize={20}
                          radiusLineColor={"#30261a"}
                          radiusLineWidth={3}
                        />
                      </div>
                      {this.state.wheel === true ? this.playwheel() : ""}
                      {/* <Wheel /> */}
                    </div>
                  </div>
                  {this.state.wheel === false ? (
                    <Sound
                      url={sound}
                      playStatus={this.state.playStatus}
                      position={this.state.position}
                      volume={this.state.volume}
                      playbackRate={this.state.playbackRate}
                      loop={this.state.loop}
                      onPlaying={({ position }) => this.setState({ position })}
                      autoLoad={true}
                    />
                  ) : null}
                  <div className="col-md-7 md-ml-4 ml-3 mt-5">
                    <table className="table  game-table">
                      <thead>
                        <tr>
                          <th
                            scope="col"
                            onClick={() => this.numbvalue("1to18")}
                          >
                            {this.state.selected.includes("1to18") === true ? (
                              <span class="chip"></span>
                            ) : null}
                            <span class="number-name">1to18</span>
                          </th>
                          <th
                            scope="col"
                            onClick={() => this.numbvalue("Even")}
                          >
                            {this.state.selected.includes("Even") === true ? (
                              <span class="chip"></span>
                            ) : null}
                            <span class="number-name">Even</span>
                          </th>
                          <th
                            scope="col"
                            className="text-danger"
                            onClick={() => this.numbvalue("Red")}
                          >
                            {this.state.selected.includes("Red") === true ? (
                              <span class="chip"></span>
                            ) : null}
                            <span class="number-name">Red</span>
                          </th>
                          <th
                            scope="col"
                            id="black"
                            className="text-dark"
                            onClick={() => this.numbvalue("Black")}
                          >
                            {this.state.selected.includes("Black") === true ? (
                              <span class="chip"></span>
                            ) : null}
                            Black
                          </th>
                          <th scope="col" onClick={() => this.numbvalue("Odd")}>
                            {this.state.selected.includes("Odd") === true ? (
                              <span class="chip"></span>
                            ) : null}
                            Odd
                          </th>
                          <th
                            scope="col"
                            onClick={() => this.numbvalue("19to36")}
                          >
                            {this.state.selected.includes("19to36") === true ? (
                              <span class="chip"></span>
                            ) : null}
                            19to36
                          </th>
                        </tr>
                      </thead>

                      <thead>
                        <tr>
                          <th
                            scope="col"
                            colSpan="2"
                            onClick={() => this.numbvalue("1st Dozen")}
                          >
                            {this.state.selected.includes("1st Dozen") ===
                            true ? (
                              <span class="chip-3"></span>
                            ) : (
                              <span class="dolly"></span>
                            )}
                            1st Dozen
                          </th>
                          <th
                            scope="col"
                            colSpan="2"
                            onClick={() => this.numbvalue("2nd Dozen")}
                          >
                            {this.state.selected.includes("2nd Dozen") ===
                            true ? (
                              <span class="chip-3"></span>
                            ) : (
                              <span class="dolly"></span>
                            )}
                            2nd Dozen
                          </th>
                          <th
                            scope="col"
                            colSpan="2"
                            onClick={() => this.numbvalue("3rd Dozen")}
                          >
                            {this.state.selected.includes("3rd Dozen") ===
                            true ? (
                              <span class="chip-3"></span>
                            ) : (
                              <span class="dolly"></span>
                            )}
                            3rd Dozen
                          </th>
                        </tr>
                      </thead>
                    </table>

                    <div className="single-number">
                      <span onClick={() => this.numbvalue("0")}>
                        {this.state.selected.includes("0") === true ? (
                          <div class="chip-5"></div>
                        ) : (
                          <div class="dolly"></div>
                        )}{" "}
                        0
                      </span>
                    </div>
                    <div className="all-numbers ">
                      <div className="numbers col-md-12 d-flex">
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("3")}
                        >
                          {this.state.selected.includes("3") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          3{" "}
                        </span>

                        <span
                          className="col text-dark"
                          id="black"
                          onClick={() => this.numbvalue("6")}
                        >
                          {this.state.selected.includes("6") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          6{" "}
                        </span>

                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("9")}
                        >
                          {this.state.selected.includes("9") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          9{" "}
                        </span>

                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("12")}
                        >
                          {this.state.selected.includes("12") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          12{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("15")}
                        >
                          {this.state.selected.includes("15") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          15{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("18")}
                        >
                          {this.state.selected.includes("18") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          18{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("21")}
                        >
                          {this.state.selected.includes("21") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          21{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("24")}
                        >
                          {this.state.selected.includes("24") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          24{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("27")}
                        >
                          {this.state.selected.includes("27") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          27{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("30")}
                        >
                          {this.state.selected.includes("30") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          30{" "}
                        </span>
                        <span
                          id="black"
                          className="col text-dark"
                          onClick={() => this.numbvalue("33")}
                        >
                          {this.state.selected.includes("33") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          33{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("36")}
                        >
                          {this.state.selected.includes("36") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          36{" "}
                        </span>
                      </div>
                    </div>
                    <div className="all-numbers ">
                      <div className="numbers col-md-12 d-flex">
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("2")}
                        >
                          {this.state.selected.includes("2") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          2{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("5")}
                        >
                          {this.state.selected.includes("5") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          5{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("8")}
                        >
                          {this.state.selected.includes("8") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          8{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("11")}
                        >
                          {this.state.selected.includes("11") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          11{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("14")}
                        >
                          {this.state.selected.includes("14") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          14{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("17")}
                        >
                          {this.state.selected.includes("17") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          17{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("20")}
                        >
                          {this.state.selected.includes("20") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          20{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("23")}
                        >
                          {this.state.selected.includes("23") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          23{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("26")}
                        >
                          {this.state.selected.includes("26") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          26{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("29")}
                        >
                          {this.state.selected.includes("29") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          29{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("32")}
                        >
                          {this.state.selected.includes("32") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          32{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("35")}
                        >
                          {this.state.selected.includes("35") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          35{" "}
                        </span>
                      </div>
                    </div>

                    <div className="all-numbers ">
                      <div className="numbers col-md-12 d-flex">
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("1")}
                        >
                          {this.state.selected.includes("1") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          1{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("4")}
                        >
                          {this.state.selected.includes("4") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          4{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("7")}
                        >
                          {this.state.selected.includes("7") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          7{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("10")}
                        >
                          {this.state.selected.includes("10") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          10{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("13")}
                        >
                          {this.state.selected.includes("13") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          13{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("16")}
                        >
                          {this.state.selected.includes("16") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          16{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("19")}
                        >
                          {this.state.selected.includes("19") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          19{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("22")}
                        >
                          {this.state.selected.includes("22") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          22{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("25")}
                        >
                          {this.state.selected.includes("25") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          25{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("28")}
                        >
                          {this.state.selected.includes("28") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          28{" "}
                        </span>
                        <span
                          className="col text-dark"
                          onClick={() => this.numbvalue("31")}
                        >
                          {this.state.selected.includes("31") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          31{" "}
                        </span>
                        <span
                          className="col text-danger"
                          onClick={() => this.numbvalue("34")}
                        >
                          {this.state.selected.includes("34") === true ? (
                            <div class="chip-2"></div>
                          ) : (
                            <div class="dolly"></div>
                          )}{" "}
                          34{" "}
                        </span>
                      </div>
                    </div>
                    <div className="rank">
                      <span onClick={() => this.numbvalue("1st")}>
                        {" "}
                        {this.state.selected.includes("1st") === true ? (
                          <div class="chip-4"></div>
                        ) : (
                          <div class="dolly"></div>
                        )}
                        1st{" "}
                      </span>
                      <span onClick={() => this.numbvalue("2nd")}>
                        {" "}
                        {this.state.selected.includes("2nd") === true ? (
                          <div class="chip-4"></div>
                        ) : (
                          <div class="dolly"></div>
                        )}
                        2nd{" "}
                      </span>
                      <span onClick={() => this.numbvalue("3rd")}>
                        {this.state.selected.includes("3rd") === true ? (
                          <div class="chip-4"></div>
                        ) : (
                          <div class="dolly"></div>
                        )}{" "}
                        3rd{" "}
                      </span>
                    </div>
                  </div>
                </div>
              </div>

              <div className="text-center btn-lg">
                {" "}
                <button className="btn btn-primary " onClick={this.clearbets}>
                  {" "}
                  Remove Bets
                </button>
                <button className="btn btn-primary " onClick={this.spinwheel}>
                  {" "}
                  Spin it!{" "}
                </button>
              </div>

              <div className="col-md-9 m-auto">
                <div className="row">
                  {this.state.selected.map((n) => {
                    return (
                      <div
                        className={
                          names.includes(n) === false ? "red-circle" : "d-none"
                        }
                      >
                        <p className="col my-2">{n}</p>
                      </div>
                    );
                  })}
                </div>
                <div className="row">
                  {this.state.selected.map((n) => {
                    return (
                      <div
                        className={
                          names.includes(n) === true ? "text-box" : "d-none"
                        }
                      >
                        <div className="">{n}</div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BetTable;
