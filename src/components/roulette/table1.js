import React, { Component } from "react";

import "../../assets/roulette/css/style.css";
// import Wheel from "./wheel";
import sound from "../../assets/roulette/sounds/chip-effect.mp3";
import wheelsound from "../../assets/roulette/sounds/ball-effect.mp3";
import logo from "../../assets/roulette/images/pointer.png";

import Sound from "react-sound";

const wheel_numbers = [
  {
    number: 0,
    color: "green",
  },
  {
    number: 32,
    color: "red",
  },
  {
    number: 15,
    color: "black",
  },
  {
    number: 19,
    color: "red",
  },
  {
    number: 4,
    color: "black",
  },
  {
    number: 21,
    color: "red",
  },
  {
    number: 2,
    color: "black",
  },
  {
    number: 25,
    color: "red",
  },
  {
    number: 17,
    color: "black",
  },
  {
    number: 34,
    color: "red",
  },
  {
    number: 6,
    color: "black",
  },
  {
    number: 27,
    color: "red",
  },
  {
    number: 13,
    color: "black",
  },
  {
    number: 36,
    color: "red",
  },
  {
    number: 11,
    color: "black",
  },
  {
    number: 30,
    color: "red",
  },
  {
    number: 8,
    color: "black",
  },
  {
    number: 23,
    color: "red",
  },
  {
    number: 10,
    color: "black",
  },
  {
    number: 5,
    color: "red",
  },
  {
    number: 24,
    color: "black",
  },
  {
    number: 16,
    color: "red",
  },
  {
    number: 33,
    color: "black",
  },
  {
    number: 1,
    color: "red",
  },
  {
    number: 20,
    color: "black",
  },
  {
    number: 14,
    color: "red",
  },
  {
    number: 31,
    color: "black",
  },
  {
    number: 9,
    color: "red",
  },
  {
    number: 22,
    color: "black",
  },
  {
    number: 18,
    color: "red",
  },
  {
    number: 29,
    color: "black",
  },
  {
    number: 7,
    color: "red",
  },
  {
    number: 28,
    color: "black",
  },
  {
    number: 12,
    color: "red",
  },
  {
    number: 35,
    color: "black",
  },
  {
    number: 3,
    color: "red",
  },
  {
    number: 26,
    color: "black",
  },
];

class BetTable1 extends Component {
  state = {
    selected: [],
    num: true,
    loop: false,
    seltext: [],
    wheel: false,
    stop: [],
    playStatus: Sound.status.PLAYING,
    position: 0,
    volume: 100,
    loop: false,
    playbackRate: 1,
    btn: "Spin it!",
  };
  spinwheel = () => {
    if (this.state.seltext.length <= 0 || this.state.selected.length <= 0) {
      this.setState({ wheel: true, btn: "Place Your Bets Please!" });
    }
    setTimeout(() => {
      this.setState({ wheel: false });
      this.setState({ btn: "Spin it!" });
      this.stopnum();
    }, 10000);
  };

  stopnum = () => {
    var item = wheel_numbers[Math.floor(Math.random() * wheel_numbers.length)];
    this.setState({ stop: item.number });
  };
  clearbets = async () => {
    this.setState({ selected: [], seltext: [], stop: [] });
  };
  numbers = () => {
    var rows = [];
    for (var i = 3; i <= 36; i++) {
      if (i % 3 === 0) {
        rows.push(
          <span
            onClick={this.numbvalue}
            id={i}
            className={
              i === this.state.stop
                ? " col winner-number text-danger"
                : i === 6 || i === 15 || i === 24 || i === 33
                ? "col text-dark winner-number"
                : "col text-danger winner-number "
            }
          >
            {this.state.selected.includes(i) === true ? (
              <span className="chip-2"></span>
            ) : i === this.state.stop ? (
              <span className="dolly ml-2"></span>
            ) : (
              ""
            )}
            {i}
          </span>
        );
      }
    }
    return rows;
  };
  numbers2 = () => {
    var rows = [];
    for (var i = 2; i <= 37; i++) {
      if (i % 3 === 0) {
        rows.push(
          <span
            onClick={this.numbvalue}
            id={i - 1}
            className={
              i - 1 === this.state.stop
                ? "col winner-number text-danger"
                : i - 1 === 2 ||
                  i - 1 === 8 ||
                  i - 1 === 11 ||
                  i - 1 === 17 ||
                  i - 1 === 20 ||
                  i - 1 === 26 ||
                  i - 1 === 29 ||
                  i - 1 === 35
                ? "col text-dark winner-number"
                : "col text-danger winner-number "
            }
          >
            {this.state.selected.includes(i - 1) === true ? (
              <span className="chip-2"></span>
            ) : i - 1 === this.state.stop ? (
              <span className="dolly ml-2"></span>
            ) : (
              ""
            )}
            {i - 1}
          </span>
        );
      }
    }
    return rows;
  };
  numbers3 = () => {
    var rows = [];
    for (var i = 2; i <= 38; i++) {
      if (i % 3 === 0) {
        rows.push(
          <span
            onClick={this.numbvalue}
            id={i - 2}
            className={
              i - 2 === this.state.stop
                ? "col winner-number text-danger"
                : i - 2 === 4 ||
                  i - 2 === 10 ||
                  i - 2 === 13 ||
                  i - 2 === 22 ||
                  i - 2 === 28 ||
                  i - 2 === 31
                ? "col text-dark winner-number"
                : "col text-danger winner-number "
            }
          >
            {this.state.selected.includes(i - 2) === true ? (
              <span className="chip-2"></span>
            ) : i - 2 === this.state.stop ? (
              <span className="dolly ml-2"></span>
            ) : (
              ""
            )}
            {i - 2}
          </span>
        );
      }
    }
    return rows;
  };
  numbvalue = async (e) => {
    if (this.state.selected.includes(parseFloat(e.target.id))) {
      var sel = this.state.selected.indexOf(parseFloat(e.target.id));
      this.state.selected.splice(sel, 1);
      await this.setState({
        selected: this.state.selected,
      });
    } else if (!this.state.selected.includes(parseFloat(e.target.id))) {
      await this.setState({
        selected: [...this.state.selected, parseFloat(e.target.id)],
      });
    }
  };
  textvalue = async (e) => {
    if (this.state.seltext.includes(e)) {
      var sel = this.state.seltext.indexOf(e);
      this.state.seltext.splice(sel, 1);
      await this.setState({
        seltext: this.state.seltext,
      });
    } else if (!this.state.seltext.includes(e)) {
      await this.setState({
        seltext: [...this.state.seltext, e],
      });
    }
  };

  render() {
    return (
      <React.Fragment>
        <div>
          <div>
            <div className="bought"></div>
            <div className="head">
              <h1 className=" text-center"> Roulette Presentation</h1>
              <h2 className=" text-center"> Try your luck!</h2>
            </div>
            <div className="bought">
              <h4 className="text-dark">
                {" "}
                You have : &nbsp; <span className="h2 text-danger"> 20 ₹ </span>
              </h4>
            </div>
            <div className="col-md-9 m-auto mobile-sm">
              <div className="game-card row pt-5 pb-5">
                {/* <img src={logo} className="arrow" /> */}
                <div className="wheel-stand col-md-3">
                  <div className="wheel-box">
                    <div className="ball-track"></div>
                  </div>
                  <div
                    className={
                      this.state.wheel === true
                        ? "r-wheel rotate-right rotate "
                        : "r-wheel rotate-right"
                    }
                  >
                    <ul>
                      {wheel_numbers.map((value, index) => {
                        return this.state.stop === value.number ? (
                          <li
                            key={index}
                            className={`number number-${value.number}`}
                            style={{ borderTopColor: "#f8bd5f" }}
                          >
                            <span className="pit">{value.number}</span>
                          </li>
                        ) : (
                          <li
                            key={index}
                            className={`number number-${value.number}`}
                            style={{ borderTopColor: value.color }}
                          >
                            <span className="pit">{value.number}</span>
                          </li>
                        );
                      })}
                    </ul>
                    {this.state.wheel === true ? (
                      <Sound
                        url={wheelsound}
                        playStatus={this.state.playStatus}
                        position={this.state.position}
                        volume={this.state.volume}
                        playbackRate={this.state.playbackRate}
                        loop={this.state.loop}
                        onPlaying={({ position }) =>
                          this.setState({ position })
                        }
                        autoLoad={true}
                      />
                    ) : null}
                    <div className="wheel-inner">
                      <div className="wheel-inner-center"></div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 m-auto mobile-sm">
                  {" "}
                  <div>
                    <table className="table  game-table">
                      <thead>
                        <tr>
                          <th
                            scope="col"
                            onClick={() => this.textvalue("1to18")}
                          >
                            {this.state.seltext.includes("1to18") === true ? (
                              <span className="chip"></span>
                            ) : null}
                            <span className="number-name">1to18</span>
                          </th>
                          <th
                            scope="col"
                            onClick={() => this.textvalue("Even")}
                          >
                            {this.state.seltext.includes("Even") === true ? (
                              <span className="chip"></span>
                            ) : null}
                            <span className="number-name">Even</span>
                          </th>
                          <th
                            scope="col"
                            className="text-danger"
                            onClick={() => this.textvalue("Red")}
                          >
                            {this.state.seltext.includes("Red") === true ? (
                              <span className="chip"></span>
                            ) : null}
                            <span className="number-name">Red</span>
                          </th>
                          <th
                            scope="col"
                            id="black"
                            className="text-dark"
                            onClick={() => this.textvalue("Black")}
                          >
                            {this.state.seltext.includes("Black") === true ? (
                              <span className="chip"></span>
                            ) : null}
                            Black
                          </th>
                          <th scope="col" onClick={() => this.textvalue("Odd")}>
                            {this.state.seltext.includes("Odd") === true ? (
                              <span className="chip"></span>
                            ) : null}
                            Odd
                          </th>
                          <th
                            scope="col"
                            onClick={() => this.textvalue("19to36")}
                          >
                            {this.state.seltext.includes("19to36") === true ? (
                              <span className="chip"></span>
                            ) : null}
                            19to36
                          </th>
                        </tr>
                      </thead>

                      <thead>
                        <tr>
                          <th
                            scope="col"
                            colSpan="2"
                            onClick={() => this.textvalue("1st Dozen")}
                          >
                            {this.state.seltext.includes("1st Dozen") ===
                            true ? (
                              <span className="chip-3"></span>
                            ) : (
                              <span className="dolly"></span>
                            )}
                            1st Dozen
                          </th>
                          <th
                            scope="col"
                            colSpan="2"
                            onClick={() => this.textvalue("2nd Dozen")}
                          >
                            {this.state.seltext.includes("2nd Dozen") ===
                            true ? (
                              <span className="chip-3"></span>
                            ) : (
                              <span className="dolly"></span>
                            )}
                            2nd Dozen
                          </th>
                          <th
                            scope="col"
                            colSpan="2"
                            onClick={() => this.textvalue("3rd Dozen")}
                          >
                            {this.state.seltext.includes("3rd Dozen") ===
                            true ? (
                              <span className="chip-3"></span>
                            ) : (
                              <span className="dolly"></span>
                            )}
                            3rd Dozen
                          </th>
                        </tr>
                      </thead>
                    </table>

                    <div
                      style={{ color: "green", fontWeight: "600" }}
                      className={
                        this.state.stop === 0
                          ? "winner-number single-number "
                          : "single-number"
                      }
                    >
                      <span onClick={() => this.textvalue("0")}>
                        {this.state.seltext.includes("0") === true ? (
                          <div className="chip-5"></div>
                        ) : this.state.stop === 0 ? (
                          <div className="dolly"></div>
                        ) : (
                          ""
                        )}
                        0
                      </span>
                    </div>
                    <div className="all-numbers ">
                      <div className="numbers col-md-12 d-flex">
                        {this.numbers()}
                      </div>
                      <div className="numbers col-md-12 d-flex">
                        {this.numbers2()}
                      </div>
                      <div className="numbers col-md-12 d-flex">
                        {this.numbers3()}
                      </div>
                      {/* <Firstrow history={this.props.history} /> */}
                    </div>

                    <div className="rank">
                      <span onClick={() => this.textvalue("1st")}>
                        {" "}
                        {this.state.seltext.includes("1st") === true ? (
                          <div className="chip-4"></div>
                        ) : (
                          <div className="dolly"></div>
                        )}
                        1st{" "}
                      </span>
                      <span onClick={() => this.textvalue("2nd")}>
                        {" "}
                        {this.state.seltext.includes("2nd") === true ? (
                          <div className="chip-4"></div>
                        ) : (
                          <div className="dolly"></div>
                        )}
                        2nd{" "}
                      </span>
                      <span onClick={() => this.textvalue("3rd")}>
                        {this.state.seltext.includes("3rd") === true ? (
                          <div className="chip-4"></div>
                        ) : (
                          <div className="dolly"></div>
                        )}{" "}
                        3rd{" "}
                      </span>
                    </div>
                  </div>
                  {this.state.wheel === false && this.state.stop.length != 0 ? (
                    <div
                      className="position-absolute"
                      style={{
                        top: "53%",
                        left: "20%",
                      }}
                    >
                      <h1 className="blink">No more bets!</h1>
                    </div>
                  ) : this.state.stop.length > 0 ? null : null}
                </div>
              </div>
            </div>
          </div>
          <div className="text-center btn-lg col-md-7 m-auto">
            {(this.state.wheel === true && this.state.selected.length >= 0) ||
            (this.state.selected && this.state.seltext.length > 0) ? (
              <button className="btn btn-primary " onClick={this.clearbets}>
                {" "}
                Remove Bets
              </button>
            ) : this.state.wheel === false &&
              this.state.selected.length <= 0 ? null : null}
            <button className="btn btn-primary " onClick={this.spinwheel}>
              {" "}
              {this.state.btn}{" "}
            </button>
          </div>
          <div className="col-md-6 m-auto">
            <div className="row">
              {this.state.selected.map((n) => {
                return (
                  <div
                    className={
                      (this.state.selected.includes(n) === true && n === 6) ||
                      n === 15 ||
                      n === 24 ||
                      n === 33 ||
                      n === 2 ||
                      n === 8 ||
                      n === 11 ||
                      n === 17 ||
                      n === 20 ||
                      n === 26 ||
                      n === 29 ||
                      n === 35 ||
                      n === 4 ||
                      n === 10 ||
                      n === 13 ||
                      n === 22 ||
                      n === 28 ||
                      n === 31
                        ? this.state.selected.includes(n) === true
                          ? "black-circle"
                          : "d-none"
                        : "red-circle"
                    }
                  >
                    <p className="col my-2">{n}</p>
                  </div>
                );
              })}
            </div>
            <div className="row">
              {this.state.seltext.map((n) => {
                return (
                  <div
                    className={
                      this.state.seltext.includes(n) === true
                        ? "text-box"
                        : "d-none"
                    }
                  >
                    <div className="">{n}</div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BetTable1;
