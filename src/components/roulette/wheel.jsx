import React, { Component } from "react";
import { Wheel } from "react-custom-roulette";
var myArray = [
  32,
  15,
  19,
  4,
  21,
  2,
  25,
  17,
  34,
  6,
  27,
  13,
  36,
  11,
  30,
  8,
  23,
  10,
  5,
  24,
  16,
  33,
  1,
  20,
  14,
  31,
  9,
  22,
  18,
  29,
  7,
  28,
  12,
  35,
  3,
  26,
];
var newarr = [
  {
    option: "0",
    style: { backgroundColor: "green", textColor: "black" },
  },
];
var arr = myArray.forEach((element) => {
  newarr.push({ option: element });
});

class New extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stopatnum: 27,
    };
  }
  render() {
    return (
      <div className="spinner-wheel">
        <Wheel
          mustStartSpinning={true}
          prizeNumber={myArray.indexOf(this.state.stopatnum) + 1}
          data={newarr}
          backgroundColors={["#3e3e3e", "#df3428"]}
          textColors={["#ffffff"]}
          textDistance={80}
          innerRadius={35}
          outerBorderWidth={10}
          innerBorderWidth={30}
          perpendicularText={true}
          fontSize={20}
          radiusLineColor={"#30261a"}
          radiusLineWidth={3}
        />
      </div>
    );
  }
}

export default New;
