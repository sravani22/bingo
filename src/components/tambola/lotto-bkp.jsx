import React, { Component } from "react";
import { connect } from "react-redux";
import Modal from "react-responsive-modal";
import auth from "../../services/authService";
import Login from "../login/login";
import clock from "../../assets/images/clock1.png";
import Moment from "react-moment";
import HowToPlay from "./howtoplay";
import moment from "moment";
// import Loader from "react-loader-spinner";
import get_lottogame from "../../redux/actions/getlottoActions";
import lotto from "../../services/lottoservice";
import { toast } from "react-toastify";
import get_lastgames from "../../redux/actions/getlastGamesActions";

class Lotto extends Component {
  state = {
    gamenum: 1,
    gamemax: 42,
    selectednum: [],
    buttondisable: false,
    popupOpen: false,
    usernum: [],
    loader: false,
  };

  async componentDidMount() {
    const { getlotto, getlastgame } = this.props;
    if (!getlotto) {
      await get_lottogame();
    }
    const user = auth.getCurrentUser();
    if (!getlastgame && user && user.email) {
      await get_lastgames();
    }
  }

  selectnum = () => {
    var rows = [];
    for (var i = 1; i <= this.state.gamemax; i++) {
      rows.push(
        <div
          className={
            this.state.selectednum.includes(i)
              ? "selectednum digit row-box mr-2 mt-1 mb-1"
              : // : this.state.usernum.length > 0
                // ? "digit row-box mr-2 mt-1 mb-1  eventsnone"
                "digit row-box mr-2 mt-1 mb-1"
          }
          id={i}
          onClick={this.selectednum}
        >
          {i}
        </div>
      );
    }
    return rows;
  };
  selectednum = async (e) => {
    if (this.state.selectednum.includes(parseFloat(e.target.id)) === true) {
      var sel = this.state.selectednum;
      sel.splice(sel.indexOf(parseFloat(e.target.id)), 1);
      await this.setState({
        selectednum: sel,
      });
    } else if (this.state.selectednum.length < 6) {
      if (!this.state.selectednum.includes(e.target.id)) {
        await this.setState({
          selectednum: [...this.state.selectednum, parseFloat(e.target.id)],
          //usernum: [...this.state.selectednum, parseFloat(e.target.id)],
        });
      }
    }
  };

  clearnums = () => {
    this.setState({ selectednum: [] });
    this.setState({ buttondisable: true });
  };
  onOpenpopupModal = () => {
    this.setState({ popupOpen: !this.state.popupOpen });
  };
  onClosepopupModal = () => {
    this.setState({ popupOpen: false });
  };

  gameSubmit = async () => {
    this.setState({ buttondisable: true });
    toast.configure();
    try {
      var obj = {
        gameid: this.props.getlotto.gameid,
        snum: this.state.selectednum,
      };
      const { data } = await lotto.sendgame(obj);

      if (data.Success) {
        this.setState({ popupOpen: false });
        toast.success(data.Success, {
          position: toast.POSITION.BOTTOM_LEFT,
        });
        // await this.setState({ usernum: this.state.selectednum });
        this.clearnums();
      }
    } catch (error) {
      if (error && error.response.status === 400) {
        this.setState({ popupOpen: false });
        toast.error(error.response.data, {
          position: toast.POSITION.BOTTOM_LEFT,
        });
        this.clearnums();
      }
    }
  };
  intersect_arrays(a, b) {
    var inclu = a.filter((word) => b.includes(word));
    return inclu;
  }
  prize = (a, b) => {
    var inclu = a.filter((word) => b.includes(word));
    if (inclu.length === 3) {
      return "100 Peso";
    } else if (inclu.length === 4) {
      return "1000 Peso";
    } else if (inclu.length === 5) {
      return "5000 Peso";
    } else if (inclu.length === 6) {
      return "10000 Peso";
    } else return "No Prize";
  };
  render() {
    const { getlotto, getlastgame } = this.props;

    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <section id="add-vertical" />
            </div>
          </div>
        </div>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="title tambola-start">
                      <div className="title-row clearfix">
                        <div className="gamenum">
                          <div className="tgame-no">
                            Game no. {getlotto ? getlotto.gameid : ""}
                          </div>
                        </div>
                        <div className="date-time">
                          <div className="game-details">
                            <div className="clearfix">
                              <div className="take-left">
                                <i className="fa fa-calendar-alt" />
                                <span className="ml-2 current-date">
                                  <Moment format="D MMM YYYY" withTitle />
                                </span>
                              </div>
                              <div className="take-right">
                                <span className="icon-t">
                                  <img src={clock} alt="time" />
                                </span>{" "}
                                <span className=" current-time">
                                  <Moment format="hh:mm A" withTitle />{" "}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tambola-start-bottom">
                      <div className="row">
                        <div className="tambola-info">
                          <h1>
                            {getlotto && getlotto.status === "pending"
                              ? "Draw At :"
                              : null}
                          </h1>
                          <div className="game-startat">
                            <p>
                              <span style={{ fontSize: "50px" }}>
                                {moment(getlotto.ndate).format(
                                  "D MMM YYYY hh:mm A"
                                )}
                              </span>
                              &nbsp;
                              <span className="time"></span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="board-border">
                      <div className=" digit_table">
                        <div className=" row ">
                          (
                          <div className="tambola-timer ml-md-5 col-md-7  ">
                            <a
                              className=""
                              data-toggle="modal"
                              data-target="#ModalBoxIndex"
                            ></a>
                            <h5 className="text-white mb-4 text-left">
                              Choose any six numbers from the below
                            </h5>
                            <div className="container mt-3 mr-4">
                              <div className="row numbers">
                                {this.selectnum()}

                                <div className="button_reset  offset-md-4">
                                  <button
                                    type="button"
                                    className="btn btn-danger bg-orange"
                                    onClick={this.clearnums}
                                  >
                                    Reset
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          )
                          <div className="tambola-timer  col-md-3 ml-md-5">
                            <h5 className="text-white mb-4">Lotto Prizes</h5>
                            <table
                              className="table  container mt-3"
                              style={{ color: "white", fontSize: "12px" }}
                            >
                              <thead>
                                <tr>
                                  <th scope="col">Matches</th>
                                  <th scope="col">Prizes</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>6/6 </td>
                                  <td>10000</td>
                                </tr>

                                <tr>
                                  <td>5/6 </td>
                                  <td>5000</td>
                                </tr>

                                <tr>
                                  <td>4/6 </td>
                                  <td>1000 </td>
                                </tr>
                                <tr>
                                  <td>3/6 </td>
                                  <td>100</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>

                        <div className="col-md-8 lottery_numbers">
                          <div className="numdisplay order-sm-2 ml-md-5 mb-md-5">
                            <h5 className="text-white mb-3 ">
                              Your Selected Numbers
                            </h5>
                            <div className="row">
                              <div className="col col-xs-3 number-box mr-2">
                                {this.state.selectednum[0]
                                  ? this.state.selectednum[0]
                                  : null}
                              </div>
                              <div className="col number-box mr-2">
                                {this.state.selectednum[1]
                                  ? this.state.selectednum[1]
                                  : null}
                                {/* {this.state.selectednum[1]
                                  ? this.state.selectednum[1]
                                  : this.state.usernum
                                  ? this.state.usernum[1]
                                  : null} */}
                              </div>
                              <div className="col number-box mr-2">
                                {this.state.selectednum[2]
                                  ? this.state.selectednum[2]
                                  : null}
                                {/* {this.state.selectednum[2]
                                  ? this.state.selectednum[2]
                                  : this.state.usernum
                                  ? this.state.usernum[2]
                                  : null} */}
                              </div>
                              <div className="col number-box mr-2">
                                {this.state.selectednum[3]
                                  ? this.state.selectednum[3]
                                  : null}
                                {/* {this.state.selectednum[3]
                                  ? this.state.selectednum[3]
                                  : this.state.usernum
                                  ? this.state.usernum[3]
                                  : null} */}
                              </div>
                              <div className="col number-box mr-2">
                                {this.state.selectednum[4]
                                  ? this.state.selectednum[4]
                                  : null}
                                {/* {this.state.selectednum[4]
                                  ? this.state.selectednum[4]
                                  : this.state.usernum
                                  ? this.state.usernum[4]
                                  : null} */}
                              </div>
                              <div className="col number-box">
                                {this.state.selectednum[5]
                                  ? this.state.selectednum[5]
                                  : null}
                                {/* {this.state.selectednum[5]
                                  ? this.state.selectednum[5]
                                  : this.state.usernum
                                  ? this.state.usernum[5]
                                  : null} */}
                              </div>

                              <div className="d-block mt-2 mb-2">
                                <button
                                  onClick={this.onOpenpopupModal}
                                  type="button"
                                  className="btn btn-success bg-pink"
                                  disabled={
                                    this.state.selectednum.length > 5
                                      ? false
                                      : true
                                  }
                                >
                                  Play Now
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-result mb-4">
                    <div className="tambolawinners-details">
                      <div className="tambola-winners-title  ">
                        <h1>Last 5 Draws</h1>
                      </div>
                      <div className="last-gamewinners">
                        <table className="table table-striped  tambola-winners-list draw-table text-center">
                          <thead>
                            <tr>
                              <th>Result Date</th>
                              <th>Numbers Drawn</th>
                              <th>Matches</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getlastgame &&
                            this.props.getlastgame.length > 0
                              ? this.props.getlastgame.map((game) => {
                                  return (
                                    <tr className="border border-bottom-secondary  ">
                                      <td className="row">
                                        <div>
                                          <i
                                            className="fa fa-calendar fw-500 fa-2x gray-text"
                                            aria-hidden="true"
                                          ></i>
                                        </div>
                                        <h6 className="game-title text-left">
                                          {moment(game.Ndate).format(
                                            "D MMM YYYY  hh:mm A"
                                          )}
                                        </h6>

                                        <span>Game Id:{game.Gameid}</span>

                                        {/* <span className="row mt-2 select-num">
                                          Your Selected Numbers
                                        </span> */}
                                      </td>
                                      <td>
                                        <div className="row digits_row">
                                          {game &&
                                            game.Drawnum.map((num) => {
                                              return (
                                                <div
                                                  className={
                                                    game.Usernum &&
                                                    game.Usernum.usnum.includes(
                                                      num
                                                    )
                                                      ? "active col col-xs-3 num-box2 mr-2 text-center px-1"
                                                      : "col col-xs-3 num-box2 mr-2 text-center px-1 "
                                                  }
                                                >
                                                  {num}
                                                </div>
                                              );
                                            })}
                                          <div className="row py-2 col-md-12 pl-0 pr-0">
                                            {game &&
                                            game.Usernum &&
                                            game.Usernum.usnum ? (
                                              <span className="select-num pl-0 ml-0">
                                                {" "}
                                                Your Picked Numbers :{" "}
                                              </span>
                                            ) : (
                                              <div className="text-center py-2  m-auto">
                                                <small className="h6">
                                                  {" "}
                                                  Not Participated{" "}
                                                </small>
                                              </div>
                                            )}

                                            {game && game.Usernum
                                              ? game &&
                                                game.Usernum &&
                                                game.Usernum.usnum.map(
                                                  (num) => {
                                                    return (
                                                      <div
                                                        className={
                                                          game.Usernum &&
                                                          game.Drawnum.includes(
                                                            num
                                                          )
                                                            ? "user-active  col col-xs-3 usernums mr-2 text-center "
                                                            : "col col-xs-3 usernums mr-2 text-center "
                                                        }
                                                      >
                                                        {num}
                                                      </div>
                                                    );
                                                  }
                                                )
                                              : null}
                                          </div>
                                        </div>
                                      </td>
                                      <td>
                                        <h4>
                                          {game &&
                                            game.Usernum &&
                                            this.intersect_arrays(
                                              game.Drawnum,
                                              game.Usernum.usnum
                                            ).length}
                                          &nbsp;{" "}
                                          {game && game.Usernum
                                            ? "Numbers"
                                            : "0 Numbers"}
                                        </h4>
                                        <h5>
                                          {game &&
                                            game.Usernum &&
                                            this.prize(
                                              game.Drawnum,
                                              game.Usernum.usnum
                                            )}{" "}
                                        </h5>
                                      </td>
                                    </tr>
                                  );
                                })
                              : null}
                          </tbody>
                        </table>
                        {this.props.getlastgame ? null : (
                          <div className="m-auto text-center py-5">
                            <h1>No Records Found </h1>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <Modal
          open={this.state.popupOpen}
          onClose={this.onOpenpopupModal}
          showCloseIcon={false}
          center
        >
          <HowToPlay close={this.onCloseplayModal} />
        </Modal> */}
        {!auth.getCurrentUser() ? (
          <Modal
            open={this.state.popupOpen}
            onClose={this.onOpenpopupModal}
            showCloseIcon={false}
            center
          >
            <Login
              loginopen={this.onOpenloginModal}
              close={this.onOpenpopupModal}
            />
          </Modal>
        ) : this.state.selectednum.length > 0 ? (
          <Modal
            open={this.state.popupOpen}
            onClose={this.onOpenpopupModal}
            center
          >
            <div onClick={this.onOpenpopupModal}>
              <div className="register_top in">
                <div className="modal-dialog" style={{ margin: "0px auto" }}>
                  <div className="modal-content">
                    <div className="modal_close">
                      <button
                        type="button"
                        className="close"
                        onClick={this.props.close}
                      >
                        ×
                      </button>
                    </div>
                    <div className="signinsignup-widget">
                      <div className="row">
                        <div className="col-sm-12 col-md-12">
                          <div className="register-left">
                            <div className="popup-title">
                              <h5
                                className="modal-title"
                                id="exampleModalLabel"
                              >
                                Are You Sure? You Want to Bet These Numbers
                              </h5>
                            </div>

                            <div className="login-form">
                              <div className="signin_username">
                                <div className="signin-form">
                                  <form className="form-horizontal">
                                    <div className="mck-form-group">
                                      <div className="row">
                                        <div className="col col-xs-3 final-display mr-2">
                                          {this.state.selectednum[0]
                                            ? this.state.selectednum[0]
                                            : null}
                                        </div>
                                        <div className="col final-display mr-2">
                                          {this.state.selectednum[1]
                                            ? this.state.selectednum[1]
                                            : null}
                                        </div>
                                        <div className="col final-display">
                                          {this.state.selectednum[2]
                                            ? this.state.selectednum[2]
                                            : null}
                                        </div>
                                      </div>
                                    </div>
                                    <div className="mck-form-group">
                                      <div className="row">
                                        <div className="col final-display mr-2">
                                          {this.state.selectednum[3]
                                            ? this.state.selectednum[3]
                                            : null}
                                        </div>
                                        <div className="col final-display mr-2">
                                          {this.state.selectednum[4]
                                            ? this.state.selectednum[4]
                                            : null}
                                        </div>
                                        <div className="col final-display">
                                          {this.state.selectednum[5]
                                            ? this.state.selectednum[5]
                                            : null}
                                        </div>
                                      </div>
                                    </div>
                                    <div className="getstarted text-center">
                                      <button
                                        // disabled={this.state.buttondisable}
                                        style={{
                                          width: "35%",
                                        }}
                                        onClick={this.gameSubmit}
                                        type="button"
                                        data-dismiss="modal"
                                        className="btn btn-success "
                                      >
                                        Yes
                                      </button>
                                      <button
                                        type="button"
                                        className="btn btn-danger ml-3"
                                        onClick={this.onClosepopupModal}
                                      >
                                        No
                                      </button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        ) : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getlotto: state.getlotto,
    balance: state.balance,
    getlastgame: state.getlastgame,
  };
};

export default connect(mapStateToProps)(Lotto);
