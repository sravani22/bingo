import React, { Component } from "react";
import { connect } from "react-redux";
import auth from "../../services/authService";
import clock from "../../assets/images/clock1.png";
import Moment from "react-moment";
import moment from "moment";
import get_lottogame from "../../redux/actions/getlottoActions";
import { toast } from "react-toastify";
import get_lastgames from "../../redux/actions/getlastGamesActions";
import { buytickets } from "../../services/lottoservice";

class Lotto extends Component {
  state = {
    loader: false,
    ticket: 0,
    disable: false,
    card: [],
  };

  async componentDidMount() {
    const { getlotto, getlastgame } = this.props;
    if (!getlotto) {
      await get_lottogame();
    }
    const user = auth.getCurrentUser();
    if (!getlastgame && user && user.email) {
      await get_lastgames();
    }
  }
  decrement = async () => {
    var dec = this.state.ticket - 1;
    await this.setState({ ticket: dec });
  };
  increment = async () => {
    var inc = this.state.ticket + 1;
    await this.setState({ ticket: inc });
  };
  boughtTickets = async () => {
    toast.configure();
    this.setState({ disable: true });
    let user = auth.getCurrentUser();
    try {
      if (!user) {
        toast.error("Please Login", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        if (user && this.state.ticket === 0) {
          toast.error("Please Select Tickets", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          const obj = {
            ntick: this.state.ticket,
            gameid: this.props.getlotto.gameid,
          };
          const data = await buytickets(obj);
          this.setState({ loader: true });
          if (data && data.data) {
            toast.success("Tickets Bought Successfully", {
              position: toast.POSITION.TOP_RIGHT,
            });
            this.setState({ card: data.data, disable: true });
          }
        }
      }
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status === 400 &&
        ex.response.status === 400
      ) {
        toast.error(ex.response.data, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    }
    await this.setState({ disable: false });
  };

  render() {
    const { getlotto } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <section id="add-vertical" />
            </div>
          </div>
        </div>
        <section id="mid-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div id="play-tambola">
                  <div className="tambola-playbox lotto">
                    <div className="title tambola-start">
                      <div className="title-row clearfix">
                        <div className="gamenum">
                          <div className="tgame-no">
                            Game no. {getlotto ? getlotto.gameid : ""}
                          </div>
                        </div>
                        <div className="date-time">
                          <div className="game-details">
                            <div className="clearfix">
                              <div className="take-left">
                                <i className="fa fa-calendar-alt" />
                                <span className="ml-2 current-date">
                                  <Moment format="D MMM YYYY" withTitle />
                                </span>
                              </div>
                              <div className="take-right">
                                <span className="icon-t">
                                  <img src={clock} alt="time" />
                                </span>{" "}
                                <span className=" current-time">
                                  <Moment format="hh:mm A" withTitle />{" "}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tambola-start-bottom">
                      <div className="row">
                        <div className="tambola-info">
                          <h1>
                            {getlotto && getlotto.status === "pending"
                              ? "Draw At :"
                              : null}
                          </h1>
                          <div className="game-startat">
                            <p>
                              <span style={{ fontSize: "50px" }}>
                                {moment(getlotto.ndate).format(
                                  "D MMM YYYY hh:mm A"
                                )}
                              </span>
                              &nbsp;
                              <span className="time"></span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="board-border">
                      <div className=" digit_table">
                        <div className=" row ">
                          <div className="tambola-timer ml-md-5 col-md-6  ">
                            <h5 className="text-white mb-4 text-center">
                              Select Number Of Tickets
                            </h5>
                            <div className="container mt-5 mr-4">
                              <div className="col-lg-8 offset-md-2">
                                <div className="input-group">
                                  {this.state.ticket <= 0 ||
                                  (this.state.card &&
                                    this.state.card.length) ? (
                                    <span className="input-group-btn">
                                      <button
                                        type="button"
                                        className="quantity-left-minus btn btn-play btn-number not-allowed"
                                      >
                                        <span className="fa fa-minus"></span>
                                      </button>
                                    </span>
                                  ) : (
                                    <span className="input-group-btn">
                                      <button
                                        type="button"
                                        className="quantity-left-minus btn btn-play  btn-number "
                                        onClick={this.decrement}
                                      >
                                        <span className="fa fa-minus"></span>
                                      </button>
                                    </span>
                                  )}
                                  <input
                                    className="form-control input-number ticket text-center"
                                    readOnly
                                    value={this.state.ticket}
                                  />
                                  {this.state.ticket === 5 ||
                                  (this.state.card &&
                                    this.state.card.length) ? (
                                    <span className="input-group-btn">
                                      <button
                                        type="button"
                                        className="quantity-right-plus btn btn-play not-allowed"
                                      >
                                        <span className="fa fa-plus"></span>
                                      </button>
                                    </span>
                                  ) : (
                                    <span className="input-group-btn">
                                      <button
                                        type="button"
                                        className="quantity-right-plus btn btn-play btn-number"
                                        onClick={this.increment}
                                      >
                                        <span className="fa fa-plus"></span>
                                      </button>
                                    </span>
                                  )}
                                </div>
                                {this.state.card && this.state.card.length ? (
                                  <button
                                    id="buy"
                                    className="btn btn-play d-block w-100 mt-3 "
                                    disabled={true}
                                  >
                                    Buy Tickets
                                  </button>
                                ) : (
                                  <button
                                    className="btn btn-play d-block w-100 mt-3"
                                    onClick={this.boughtTickets}
                                    disabled={this.state.disable}
                                  >
                                    Buy Tickets
                                  </button>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className="tambola-timer  col-md-3 ml-md-5">
                            <h5 className="text-white mb-4">Lotto Prizes</h5>
                            <table
                              className="table  container mt-3"
                              style={{ color: "white", fontSize: "12px" }}
                            >
                              <thead>
                                <tr>
                                  <th scope="col">Matches</th>
                                  <th scope="col">Prizes</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>9/9 </td>
                                  <td>10000</td>
                                </tr>
                                <tr>
                                  <td>8/9 </td>
                                  <td>5000</td>
                                </tr>

                                <tr>
                                  <td>7/9 </td>
                                  <td>1000 </td>
                                </tr>
                                <tr>
                                  <td>6/9 </td>
                                  <td>100</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {this.state.loader === true ? (
              <div id="play-tambola">
                <div className="lotto tambola-playbox mx-3">
                  <div className="board-border">
                    <div className="col-md-12">
                      <div className=" row ">
                        <div className="col-md-12">
                          {this.state.card && this.state.card.length ? (
                            <div class="row justify-content-center">
                              {this.state.card.map((c, index) => (
                                <div className="col-md-3 ">
                                  <table className="bingo_box border-r mt-2 mb-3 pr-2">
                                    <thead>
                                      <th
                                        className="digit  mr-2 mt-1 mb-1"
                                        colSpan="3"
                                      >
                                        Ticket {index + 1}
                                      </th>
                                    </thead>

                                    <tbody>
                                      <tr>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[0]}
                                        </td>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[1]}
                                        </td>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[2]}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[3]}
                                        </td>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[4]}
                                        </td>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[5]}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[6]}
                                        </td>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[7]}
                                        </td>
                                        <td className="digit row-box_2 mr-2 py-2">
                                          {c[8]}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              ))}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getlotto: state.getlotto,
    balance: state.balance,
    getlastgame: state.getlastgame,
  };
};

export default connect(mapStateToProps)(Lotto);
