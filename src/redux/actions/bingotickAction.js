import store from "../index";
import bingo from "../../services/bingoservice";

export var GET_BINGOTICK = "GET_BINGOTICK";

function get_bingotick() {
  setTimeout(async () => {
    try {
      const { data } = await bingo.gettickets();
      store.dispatch({ type: GET_BINGOTICK, payload: data });
    } catch (error) {}
  }, 100);
}

export default get_bingotick;
