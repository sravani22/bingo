import store from "../index";
import bingo from "../../services/bingoservice";

export var GET_BINGOGAME = "GET_BINGOGAME";

function get_bingogame() {
  setTimeout(async () => {
    try {
      const { data } = await bingo.getgamebingo();
      store.dispatch({ type: GET_BINGOGAME, payload: data });
    } catch (error) {}
  }, 100);
}

export default get_bingogame;
