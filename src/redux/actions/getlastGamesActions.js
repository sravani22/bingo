import store from "../index";
import lotto from "../../services/lottoservice";
export var GET_LASTGAME = "GET_LASTGAME";

function get_lastgames() {
  setTimeout(async () => {
    try {
      const { data } = await lotto.getLastGames();
      store.dispatch({ type: GET_LASTGAME, payload: data });
    } catch (error) {}
  }, 100);
}

export default get_lastgames;
