import store from "../index";
import lotto from "../../services/lottoservice";
export var GET_LOTTOGAME = "GET_LOTTOGAME";

function get_lottogame() {
  setTimeout(async () => {
    try {
      const { data } = await lotto.getgamelotto();
      store.dispatch({ type: GET_LOTTOGAME, payload: data });
    } catch (error) {}
  }, 100);
}

export default get_lottogame;
