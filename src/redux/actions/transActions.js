import store from '../index';
import auth from '../../services/authService';
export var GET_TRANS = 'GET_TRANS';

function get_trans() {
	setTimeout(async () => {
		try {
			const { data } = await auth.trans();
			store.dispatch({ type: GET_TRANS, payload: data.success });
		} catch (error) {}
	}, 100);
}

export default get_trans;
