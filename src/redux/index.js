import { createStore, applyMiddleware, compose } from 'redux';
import allReducers from '../redux/reducers/index';
import thunk from 'redux-thunk';

const myreducers = allReducers();
const middleware = [ thunk ];
let enhancers = compose(applyMiddleware(thunk));
let store = createStore(myreducers, compose(enhancers, applyMiddleware(...middleware)));
export default store;
