import { GET_BINGOGAME } from "../actions/getbingoActions";

const getbingoReducer = (state = "", { type, payload }) => {
  switch (type) {
    case GET_BINGOGAME:
      return payload;
    default:
      return state;
  }
};
export default getbingoReducer;
