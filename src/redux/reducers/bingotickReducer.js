import { GET_BINGOTICK } from "../actions/bingotickAction";

const bingotickReducer = (state = null, { type, payload }) => {
  switch (type) {
    case GET_BINGOTICK:
      return payload;
    default:
      return state;
  }
};
export default bingotickReducer;
