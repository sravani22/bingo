import { GET_GAMETICK } from '../actions/gametickActions';

const gametickReducer = (state = null, { type, payload }) => {
	switch (type) {
		case GET_GAMETICK:
			return payload;
		default:
			return state;
	}
};
export default gametickReducer;
