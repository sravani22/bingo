import { GET_LASTGAME } from "./../actions/getlastGamesActions";

const getlastgameReducer = (state = "", { type, payload }) => {
  switch (type) {
    case GET_LASTGAME:
      return payload;
    default:
      return state;
  }
};
export default getlastgameReducer;
