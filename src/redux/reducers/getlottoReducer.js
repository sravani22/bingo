import { GET_LOTTOGAME } from "../actions/getlottoActions";

const getlottoReducer = (state = "", { type, payload }) => {
  switch (type) {
    case GET_LOTTOGAME:
      return payload;
    default:
      return state;
  }
};
export default getlottoReducer;
