import http from "./httpservice";
const [apiEndpoint, tokenKey] = ["/bingo", "token"];

export function getgamebingo(user) {
  return http.post(apiEndpoint + "/get", user);
}
export function sendgame(obj) {
  return http.post(apiEndpoint + "/ticket", obj);
}
export function gettickets() {
  return http.post(apiEndpoint + "/getticket");
}
export function drawnumbers(obj) {
  return http.post(apiEndpoint + "/getnumbingo", obj);
}
export function gamebingo(obj) {
  return http.post(apiEndpoint + "/win", obj);
}

export default {
  getgamebingo,
  sendgame,
  gettickets,
  drawnumbers,
  gamebingo,
};
