import http from "./httpservice";
const [apiEndpoint, tokenKey] = ["/lotto", "token"];

export function getgamelotto(user) {
  return http.post(apiEndpoint + "/get", user);
}
export function sendgame(obj) {
  return http.post(apiEndpoint + "/unum", obj);
}
export function getLastGames() {
  return http.post(apiEndpoint + "/last");
}

export function buytickets(obj) {
  return http.post(apiEndpoint + "/unum", obj);
}
export default {
  getgamelotto,
  sendgame,
  getLastGames,
  buytickets,
};
